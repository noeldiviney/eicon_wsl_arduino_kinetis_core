



#ifndef _gTuner_h_
	#define _gTuner_h_
	#ifdef __cplusplus

		#include <inttypes.h>
		#include "Arduino.h"


#ifdef __cplusplus
extern "C" {
#endif

void gasmFtm1_isr(void);

#ifdef __cplusplus
}
#endif


class gTuner2
{
  public:
	gTuner2(uint8_t pin_num) { begin(pin_num); }
	uint16_t read(void);
	uint8_t* noteName(uint8_t noteNum);
	void tolerance(float newTol);
	void startISR(void);
	
	float freq(void);
	float diff(void);
	uint8_t octave(void);
	
	uint32_t* bitTest(int32_t ptroff);
	uint32_t* ptrTest(int32_t ptroff);
	
	uint16_t result(float freq);
	
  protected:
	
	
  private:
	void begin(uint8_t pin_num);
	uint8_t _pin_num=0;
	
};



	#endif // __cplusplus
#endif // gTuner_h
