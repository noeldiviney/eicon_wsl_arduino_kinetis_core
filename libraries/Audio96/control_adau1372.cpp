
#include "Wire.h"
#include "control_adau1372.h"

// adau1372 register defines
#define CLK_CONTROL 0x0
#define PLL_CTRL0 0x1
#define PLL_CTRL1 0x2
#define PLL_CTRL2 0x3
#define PLL_CTRL3 0x4
#define PLL_CTRL4 0x5
#define PLL_CTRL5 0x6
#define CLKOUT_SEL 0x7
#define REGULATOR 0x8
#define DAC_SOURCE_0_1 0x11
#define SOUT_SOURCE_0_1 0x13
#define SOUT_SOURCE_2_3 0x14
#define SOUT_SOURCE_4_5 0x15
#define SOUT_SOURCE_6_7 0x16
#define ADC_SDATA_CH 0x17
#define ASRCO_SOURCE_0_1 0x18
#define ASRCO_SOURCE_2_3 0x19
#define ASRC_MODE 0x1A
#define ADC_CONTROL0 0x1B
#define ADC_CONTROL1 0x1C
#define ADC_CONTROL2 0x1D
#define ADC_CONTROL3 0x1E
#define ADC0_VOLUME 0x1F
#define ADC1_VOLUME 0x20
#define ADC2_VOLUME 0x21
#define ADC3_VOLUME 0x22
#define PGA_CONTROL_0 0x23
#define PGA_CONTROL_1 0x24
#define PGA_CONTROL_2 0x25
#define PGA_CONTROL_3 0x26
#define PGA_STEP_CONTROL 0x27
#define PGA_10DB_BOOST 0x28
#define POP_SUPPRESS 0x29
#define TALKTHRU 0x2A
#define TALKTHRU_GAIN0 0x2B
#define TALKTHRU_GAIN1 0x2C
#define MIC_BIAS 0x2D
#define DAC_CONTROL1 0x2E
#define DAC0_VOLUME 0x2F
#define DAC1_VOLUME 0x30
#define OP_STAGE_MUTES 0x31
#define SAI_0 0x32
#define SAI_1 0x33
#define SOUT_CONTROL0 0x34
#define MODE_MP0 0x38
#define MODE_MP1 0x39
#define MODE_MP4 0x3C
#define MODE_MP5 0x3D
#define MODE_MP6 0x3E
#define PB_VOL_SET 0x3F
#define PB_VOL_CONV 0x40
#define DEBOUNCE_MODE 0x41
#define OP_STAGE_CTRL 0x43
#define DECIM_PWR_MODES 0x44
#define INTERP_PWR_MODES 0x45
#define BIAS_CONTROL0 0x46
#define BIAS_CONTROL1 0x47
#define PAD_CONTROL0 0x48
#define PAD_CONTROL1 0x49
#define PAD_CONTROL2 0x4A
#define PAD_CONTROL3 0x4B
#define PAD_CONTROL4 0x4C
#define PAD_CONTROL5 0x4D
#define ADC_OPER 0x60



	unsigned char AudioControlADAU1372::writeReg(uint8_t regNum, uint8_t value)
	{
		Wire.beginTransmission(adau1372_I2C);
		Wire.write(regNum >> 8);
		Wire.write(regNum);
		Wire.write(value);
		return Wire.endTransmission();
//		Serial.printf("writeReg(%U,%U) [%U]\n",regNum,value,Wire.endTransmission());
		return 0;
	}
	
	unsigned char AudioControlADAU1372::readReg(uint8_t regNum)
	{
		Wire.beginTransmission(adau1372_I2C);
		Wire.write(regNum >> 8);
		Wire.write(regNum);
		Wire.endTransmission();
		if(Wire.requestFrom(adau1372_I2C,1)!=1) return 255;
		return Wire.read();
	}

	bool AudioControlADAU1372::enableSlave(uint8_t power_down_pin)
	{
		Wire.begin();
		pinMode(power_down_pin,OUTPUT);
		digitalWrite(power_down_pin,HIGH);
		delay(25); 
		writeReg(CLK_CONTROL,0x3);
		delay(125); // this was four hundred but I suspect me of being over-zealous there.
		// writeReg(PLL_CTRL0,0x0); // not using PLL
		writeReg(PLL_CTRL1,0x1);
		// writeReg(PLL_CTRL2,0x0);
		// writeReg(PLL_CTRL3,0x0);
		writeReg(PLL_CTRL4,0x20); 
		// writeReg(PLL_CTRL5,0x1); // read only flag for PLL lock - not using PLL -wtf sigma studio?
		writeReg(CLKOUT_SEL,0x7); // no clkout; master clock divide by 4 = 3.072Mhz
		// writeReg(REGULATOR,0x0);
		writeReg(DAC_SOURCE_0_1,0xDC);
		// writeReg(SOUT_SOURCE_0_1,0x54); // reset value
		// writeReg(SOUT_SOURCE_2_3,0x76); // reset value
		// writeReg(SOUT_SOURCE_4_5,0x54); // reset value
		//  writeReg(SOUT_SOURCE_6_7,0x76); // reset value
		// writeReg(ADC_SDATA_CH,0x4); // reset value
		writeReg(ASRCO_SOURCE_0_1,0x54); // ADC_0 and ADC_1
		writeReg(ASRCO_SOURCE_2_3,0x76); // ADC_2 and NONE - was 0x36
		writeReg(ASRC_MODE,0x3); // ASRC out & in enabled
		writeReg(ADC_CONTROL0,0x0); // ADC_0 & ADC_1 not muted, @ 96K
		writeReg(ADC_CONTROL1,0x10); // ADC_2 not muted, ADC_3 muted, @96K
		writeReg(ADC_CONTROL2,0x43); // ADC_0 & ADC_1 hi-pass @4Hz, enabled.
		writeReg(ADC_CONTROL3,0x41); // ADC_2 & ADC_3 hi-pass @4Hz, ADC_2 enabled, ADC_3 disabled.
		// writeReg(ADC0_VOLUME,0x0); // ADC_0 vol=0dB is reset value
		// writeReg(ADC1_VOLUME,0x0); // ADC_1 vol=0dB is reset value
		// writeReg(ADC2_VOLUME,0x0); // ADC_2 vol=0dB is reset value
		// writeReg(ADC3_VOLUME,0x0); // ADC_3 vol=0dB is reset value
		writeReg(PGA_CONTROL_0,0x10); // reset value is 0x0 which is PGA disabled
		writeReg(PGA_CONTROL_1,0x10); // reset value is 0x0 which is PGA disabled
		writeReg(PGA_CONTROL_2,0x10); // reset value is 0x0 which is PGA disabled
		writeReg(PGA_CONTROL_3,0x10); // reset value is 0x0 which is PGA disabled
		// writeReg(PGA_STEP_CONTROL,0x0); // reset value
		// writeReg(PGA_10DB_BOOST,0x0); // reset value
		writeReg(POP_SUPPRESS,0x0); // enabling all pop suppression
		// writeReg(TALKTHRU,0x0); // reset value
		// writeReg(TALKTHRU_GAIN0,0x0); // reset value
		// writeReg(TALKTHRU_GAIN1,0x0); // reset value
		writeReg(MIC_BIAS,0x10); // MIC_BIAS_0 at 0.9 * AVDD
		writeReg(DAC_CONTROL1,0x11); // DAC_0 enabled not muted, DAC_1 disabled and muted
		// writeReg(DAC0_VOLUME,0x0); // reset value - DAC_0 Volume 0dB
		// writeReg(DAC1_VOLUME,0x0); // reset value
		writeReg(SAI_0,0x06); // TDM, I2S data delayed from edge of LRCLK by 1 BCLK cycle; Stereo (I2S, left justified, right justified); @ 96K
		writeReg(SAI_1,0x45); // BCLK_TDMC 16 bit data even though mode is not TDM, BCLKRATE=16 bits per channel, LRCLK/BCLK=Master
		writeReg(SOUT_CONTROL0,0xF0); // reset value
		// writeReg(MODE_MP0,0x0); // reset value - Serial Input
		writeReg(MODE_MP1,0x0); // Serial Out 0 selected for this pin
		// writeReg(MODE_MP4,0x0); // reset value - digital microphones
		// writeReg(MODE_MP5,0x0); // reset value - digital microphones
		writeReg(MODE_MP6,0x0); // Serial Out 1 selected for this pin
		// writeReg(PB_VOL_SET,0x0); // reset value
		// writeReg(PB_VOL_CONV,0x87); // reset value
		// writeReg(DEBOUNCE_MODE,0x5); // reset value
		writeReg(OP_STAGE_MUTES,0x0F); // all muted...
		writeReg(OP_STAGE_CTRL,0x10); // HPR/LOR powered down, HPL/LOL powered up
		writeReg(DECIM_PWR_MODES,0x77); // ADC_0 to ADC_2 decimator and filter powered up, ADC_3 decimator and filter powered down.
		writeReg(INTERP_PWR_MODES,0x5); // DAC_1 interpolator disabled, DAC_0 interpolator enabled, ASRC interpolator enabled
		writeReg(BIAS_CONTROL0,0x0); // headphone bias in extreme power saving mode.
		// writeReg(BIAS_CONTROL1,0x0); // reset value
		// writeReg(PAD_CONTROL0,0x7F); // reset value - no pullups
		// writeReg(PAD_CONTROL1,0x1F); // reset value - no pullups
		// writeReg(PAD_CONTROL2,0x0); // reset value - no pulldowns
		// writeReg(PAD_CONTROL3,0x0); // reset value - no pulldowns
		// writeReg(PAD_CONTROL4,0x0); // reset value - low drive strength
		// writeReg(PAD_CONTROL5,0x0); // reset value - low drive strength
		writeReg(ADC_OPER,0xA0);
		delay(6); // wait at least 6 milliseconds before unmuting
		return true;
	}

	void AudioControlADAU1372::inputControl(bool in0, bool in1, bool in2, bool in3)
	{
		uint8_t adc_control2=96,adc_control3=96,decim_pwr_modes=0;
		if(in0)
		{
			adc_control2|=1;
			decim_pwr_modes|=17;
		}
		
		if(in1)
		{
			adc_control2|=2;
			decim_pwr_modes|=34;
		}
		
		if(in2)
		{
			adc_control3|=1;
			decim_pwr_modes|=68;
		}
		
		if(in3)
		{
			adc_control3|=2;
			decim_pwr_modes|=136;
		}
		
		writeReg(ADC_CONTROL2,adc_control2);
		writeReg(ADC_CONTROL3,adc_control3);
		writeReg(DECIM_PWR_MODES,decim_pwr_modes);
	}
	
	void AudioControlADAU1372::inputControl(uint8_t inNum, bool state)
	{
		uint8_t adc_control2=readReg(ADC_CONTROL2), adc_control3=readReg(ADC_CONTROL3), decim_pwr_modes=readReg(DECIM_PWR_MODES);
		uint8_t ac2=adc_control2,ac3=adc_control3,dpm=decim_pwr_modes,ac2a=0,ac3a=0,dpma=0;
		
		switch(inNum)
		{
		case 3:
			ac3a=2;
			dpma=136;
		break;
		case 1:
			ac2a=2;
			dpma=34;
		break;
		case 2:
			ac3a=1;
			dpma=68;
		break;
		default:
			ac2a=1;
			dpma=17;
		}
		if(state)
		{
			ac2|=ac2a;
			ac3|=ac3a;
			dpm|=dpma;
		} else {
			ac2&=~ac2a;
			ac3&=~ac3a;
			dpm&=~dpma;
		}
		if(adc_control2!=ac2) writeReg(ADC_CONTROL2,ac2);
		if(adc_control3!=ac3) writeReg(ADC_CONTROL3,ac3);
		if(decim_pwr_modes!=dpm) writeReg(DECIM_PWR_MODES,dpm);
	}

	void AudioControlADAU1372::inputLevel(uint8_t inNum, float setting)
	{
		if(setting>1) setting=1;
		if(setting<0) setting=0;
		setting=(1-setting)*255;
		writeReg(ADC0_VOLUME+inNum,(uint8_t)setting);
	}
	
	void AudioControlADAU1372::inputLevel(uint8_t inNum, uint8_t setting)
	{
		writeReg(ADC0_VOLUME+inNum,setting);
	}
	
	
	void AudioControlADAU1372::outputControl(bool left, bool right)
	{
		writeReg(OP_STAGE_MUTES,15); // hard mute all outputs
		uint8_t dac_control1=0,op_stage_ctrl=0,interp_pwr_modes=0/*,op_stage_mutes=0*/;
		if(left)
		{
			dac_control1|=1;
			interp_pwr_modes|=5;
		} else {
			dac_control1|=8;
			op_stage_ctrl|=3;
			// op_stage_mutes|=3;
		}
		
		if(right)
		{
			dac_control1|=2;
			interp_pwr_modes|=10;
		} else {
			dac_control1|=16;
			op_stage_ctrl|=12;
			// op_stage_mutes|=12;
		}
		writeReg(DAC_CONTROL1,dac_control1);
		writeReg(OP_STAGE_CTRL,op_stage_ctrl);
		writeReg(INTERP_PWR_MODES,interp_pwr_modes);
		delay(6);
		writeReg(OP_STAGE_MUTES,op_stage_ctrl); // ends up with same values.
	}
	
	void AudioControlADAU1372::outputControl(uint8_t outNum, bool state)
	{
		uint8_t dac_control1=readReg(DAC_CONTROL1),op_stage_ctrl=readReg(OP_STAGE_CTRL),interp_pwr_modes=readReg(INTERP_PWR_MODES),op_stage_mutes=readReg(OP_STAGE_MUTES);
		uint8_t dc1=dac_control1,osc=op_stage_ctrl,ipm=interp_pwr_modes;
		uint8_t dc1t=1<<outNum,dc1f=8<<outNum,oscf=3<<(outNum*2),ipmt=5<<outNum;
		writeReg(OP_STAGE_MUTES,op_stage_mutes|oscf); // mute the output in question
		
		if(state)
		{
			dc1|=dc1t;
			dc1&=~dc1f;
			osc&=~oscf;
			ipm|=ipmt;
		} else {
			dc1&=~dc1t;
			dc1|=dc1f;
			osc|=oscf;
			ipm&=~ipmt;
		}

		if(dac_control1!=dc1) writeReg(DAC_CONTROL1,dc1);
		if(op_stage_ctrl!=osc) writeReg(OP_STAGE_CTRL,osc);
		if(interp_pwr_modes!=ipm) writeReg(INTERP_PWR_MODES,ipm);
		if(op_stage_mutes!=ipm)
		{
			delay(6);
			writeReg(OP_STAGE_MUTES,osc); // ends up with same values.
		}
	}
	
	void AudioControlADAU1372::outputLevel(uint8_t outNum, float setting)
	{
		if(setting>1) setting=1;
		if(setting<0) setting=0;
		setting=(1-setting)*255;
		writeReg(DAC0_VOLUME+outNum,(uint8_t)setting);
	}
	void AudioControlADAU1372::outputLevel(uint8_t outNum, uint8_t setting)
	{
		writeReg(DAC0_VOLUME+outNum,setting);
	}
	
	void AudioControlADAU1372::pgaSlewRate(uint8_t slewRate)
	{
		slewRate&=3;
		if(slewRate==3) slewRate=2;
		uint8_t pga_step_control=readReg(PGA_STEP_CONTROL)&~48;
		writeReg(PGA_STEP_CONTROL,pga_step_control|(slewRate<<4));
	}
	void AudioControlADAU1372::pgaEnable(uint8_t chanNum, bool state)
	{
		uint8_t pga_control_x=readReg(PGA_CONTROL_0+chanNum)&127;
		writeReg(PGA_CONTROL_0+chanNum,pga_control_x|(state<<7));
	}
	
	void AudioControlADAU1372::pgaBoost(uint8_t chanNum, bool boost)
	{
		uint8_t pga_10db_boost=readReg(PGA_10DB_BOOST)&~(1<<chanNum);
		writeReg(PGA_10DB_BOOST,pga_10db_boost|(boost<<chanNum));
	}
	
	void AudioControlADAU1372::pgaSlew(uint8_t chanNum, bool slew)
	{
		uint8_t pga_step_control=readReg(PGA_STEP_CONTROL)&~(1<<chanNum);
		writeReg(PGA_STEP_CONTROL,pga_step_control|(slew<<chanNum));
	}
	
	void AudioControlADAU1372::pgaLevel(uint8_t chanNum, uint8_t level)
	{
		uint8_t pga_control_x=readReg(PGA_CONTROL_0+chanNum)&128;
		writeReg(PGA_CONTROL_0+chanNum,pga_control_x|level);
	}

	void AudioControlADAU1372::micBias(uint8_t chanNum, uint8_t level)
	{
		uint8_t mic_bias=readReg(MIC_BIAS)&~(17<<chanNum);
		if(level) mic_bias|=(16<<chanNum)|((level&1)<<chanNum);
		writeReg(MIC_BIAS,mic_bias);
	}
	void AudioControlADAU1372::passThru(bool en)
	{
		uint8_t tt=readReg(TALKTHRU)&~3;
		if(en) tt|=3; // ADC0->DAC0; ADC1->DAC1
		writeReg(TALKTHRU,tt);
		writeReg(TALKTHRU_GAIN0,0);
		writeReg(TALKTHRU_GAIN1,0);
	}
	