
#include "Wire.h"
#include "tlv320.h"

/* Register definitions */
#include "tlv320regs.h"

typedef union {
  // uint16_t value;
  unsigned short value;
  struct {
    // uint8_t lo;
	unsigned char lo;
    // uint8_t hi;
	unsigned char hi;
  };
} uconvert16to8s;

typedef union {
  // uint32_t value;
  unsigned long value;
  // struct {
    // uint8_t d[4];
	unsigned char d[4];
  // };
} uconvert32to8s;

typedef union {
	int8_t ns;
	uint8_t nu;
} s8convertu8;

// MCLK needs to be 48e6 / 1088 * 256 = 11.29411765 MHz -> 44.117647 kHz sample rate
//
#if F_CPU == 96000000 || F_CPU == 48000000 || F_CPU == 24000000
  // PLL is at 96 MHz in these modes
  #define MCLK_MULT 2
  #define MCLK_DIV  17
#elif F_CPU == 72000000
  #define MCLK_MULT 8
  #define MCLK_DIV  51
#elif F_CPU == 120000000
  #define MCLK_MULT 8
  #define MCLK_DIV  85
#elif F_CPU == 144000000
  #define MCLK_MULT 4
  #define MCLK_DIV  51
#elif F_CPU == 168000000
  #define MCLK_MULT 8
  #define MCLK_DIV  119
#elif F_CPU == 16000000
  #define MCLK_MULT 12
  #define MCLK_DIV  17
#else
  #error "This CPU Clock Speed is not supported by the Audio library";
#endif

#if F_CPU >= 20000000
  #define MCLK_SRC  3  // the PLL
#else
  #define MCLK_SRC  0  // system clock
#endif


	void AudioControlTLV320::chkPage(unsigned char page)
	{
		if(page!=curPage)
		{
			Wire.beginTransmission(TLV320xxx_I2C);
			Wire.write(0);
			Wire.write(page);
			curPage=page;
			Wire.endTransmission();
		}
	}

	unsigned char AudioControlTLV320::writeReg(unsigned char page, unsigned char reg, unsigned char val)
	{
		chkPage(page);
		Wire.beginTransmission(TLV320xxx_I2C);
		Wire.write(reg);
		Wire.write(val);
		return Wire.endTransmission();
	}
	unsigned char AudioControlTLV320::readReg(unsigned char page, unsigned char reg)
	{
		chkPage(page);
		Wire.beginTransmission(TLV320xxx_I2C);
		Wire.write(reg);
		Wire.endTransmission();
		if(Wire.requestFrom(TLV320xxx_I2C,1)!=1) return 0;
		return Wire.read();
		
	}

//	bool AudioControlTLV320::enableSlave(uint8_t decimation, uint8_t interpolation)
	bool AudioControlTLV320::enableSlave(uint8_t reset_pin, uint8_t decimation, uint8_t interpolation)
	{

		// enable MCLK output
		// Serial.println("I2S0_MCR = I2S_MCR_MICS(MCLK_SRC) | I2S_MCR_MOE;");
		I2S0_MCR = I2S_MCR_MICS(MCLK_SRC) | I2S_MCR_MOE;
		// Serial.println("I2S_MDR_FRACT((MCLK_MULT-1)) | I2S_MDR_DIVIDE((MCLK_DIV-1));");
		I2S0_MDR = I2S_MDR_FRACT((MCLK_MULT-1)) | I2S_MDR_DIVIDE((MCLK_DIV-1));
		delay(25);
		pinMode(reset_pin,OUTPUT);
		digitalWrite(reset_pin,HIGH);
		delay(175);
		curPage=255;
	// software reset
		
		// Serial.println("writeReg(SRR,1);");
		writeReg(SRR,1); // selected page 0, wrote '1' to register 1 to soft reset.
		// Serial.println("delay(100);");
		delay(250); // the device is resetting, wait...
		
		/*
			PLL_CLOCKOUT = 98304000
			PLL_CLOCKIN = 11294117.65
			
			PLL_CLOCKOUT = PLL_CLOCKIN*R*JD/P
		
			98304000/11294117.65 = 8.7039999977333333339236111109574 (*2 = 17.407999995466666667847222221915, rounding to 17.408 perhaps.
			
			R=2
			J.D=8.704 :: J=8, D[MSB]=2, D[LSB]=192
			P=2
			
			alternative:
			
			R=1, J.D=8.704, P=1
		*/
		
		// Serial.println("Wire.beginTransmission(TLV320xxx_I2C);");
		Wire.beginTransmission(TLV320xxx_I2C);
		// Serial.print("<");
		Wire.write(4); // select register 4 for start of writes.
		// Serial.print(".");
		Wire.write((1<<6)|3); // selecting MCLK as input to PLL, PLL as input to CODEC_CLKIN - high PLL range
		// Serial.print(".");

		// MCLK=11294117.65
		Wire.write((1<<7)|(1<<4)|1); // P & R values to register 5 // 1<<7 = PLL enabled, 1<<4 = P value=1, 1= R value=1
		// Serial.print(".");
		Wire.write(8); // J value to register 6
		// Serial.print(".");
		Wire.write(27); // MSB of D value to register 7
		// Serial.print(".");
		Wire.write(128); // LSB of D value to register 8
		// Serial.print(".");


		Wire.write(0); // skip register 9
		// Serial.print(".");
		Wire.write(0); // skip register 10
		// Serial.print(".");
		Wire.write((1<<7)|4); // NDAC value to register 11, divider powered on
		// Serial.print(".");
		Wire.write((1<<7)|4); // MDAC value to register 12, divider powered on
		// Serial.print(".");
		Wire.write(0); // DOSR value (MSB) to register 13
		// Serial.print(".");
		Wire.write(64); // DOSR value (LSB) to register 14
		// Serial.print(">");
		Wire.endTransmission(); 
		// Serial.println(" Done.) ");
		
	// I2S set up
		// writeReg(AISR1,0); // Audio Interface: I2S, 16bit, BCLK is input, WCLK is input, DOUT always driven - the default set up.
		writeReg(AISR1,(3<<2)); // Audio Interface: I2S, 16bit, BCLK is output, WCLK is output, DOUT always driven - not the default set up.
		writeReg(AISR3,(0<<2)|2); // BCLK & WCLK buffers !always powered up, using DAC_CLOCK for BDIV_CLKIN = 6144000
		writeReg(CSR12,(1<<7)|8); // BCLK N divider powered up, BCLK N divider = 2
		
		writeReg(CSR10,3); // CDIV_CLKIN = PLL
		writeReg(CSR11,129); // M divider powered up and equ 1
		writeReg(MFP5CR,0); // Not CLKOUT because not configured
		
		
		
		
		Wire.beginTransmission(TLV320xxx_I2C);
		Wire.write(18); // select register 18 for start of next writes
		Wire.write((0<<7)|4); // NADC value to register 18, divider off
		Wire.write((0<<7)|4); // MADC value to register 19, divider off
		Wire.write(64); // AOSR value to register 20
		Wire.endTransmission();
		
	// processing blocks or miniDSP
		Wire.beginTransmission(TLV320xxx_I2C);
		Wire.write(60); // select register 60, we are on page 0
/*		
Proc	Interpolation	Channel	1st Order	Num. 	DRC	3D	Beep		Resource
Block	Filter					IIR 		Biquads			Generator	Class
--------------------------------------------------------------------------------
PRB_P7		B			Stereo	Yes			0		No	No	No			6
PRB_P8		B			Stereo	No			4		Yes	No	No			8
PRB_P9		B			Stereo	No			4		No	No	No			8
PRB_P10		B			Stereo	Yes			6		Yes	No	No			10
PRB_P11		B			Stereo	Yes			6		No	No	No			8
PRB_P12		B			Left	Yes			0		No	No	No			3
PRB_P13		B			Left	No			4		Yes	No	No			4
PRB_P14		B			Left	No			4		No	No	No			4 < -- CURRENT SELECTION
PRB_P15		B			Left	Yes			6		Yes	No	No			6
PRB_P16		B			Left	Yes			6		No	No	No			4

PRB_P17 	C 			Stereo	Yes			0		No	No	No			3
PRB_P18		C			Stereo	Yes			4		Yes	No	No			6 < -- previous selection
PRB_P19		C			Stereo	Yes			4		No	No	No			4
PRB_P20		C			Left	Yes			0		No	No	No			2
PRB_P21		C			Left	Yes			4		Yes	No	No			3
PRB_P22		C			Left	Yes			4		No	No	No			2
*/
		Wire.write(interpolation); // 18 or 8
/*
Proc	Channel	Decimation	1st Order	Number	FIR		Required	Resource
Block			Filter		IIR			BiQuads			AOSR Value	Class
----------------------------------------------------------------------------
PRB_R7	Stereo		B		Yes			0		No		64			3
PRB_R8	Stereo		B		Yes			3		No		64			4 <-- using this one atm.
PRB_R9	Stereo		B		Yes			0		20-Tap	64			4

PRB_R13	Stereo		C		Yes			0		No		32			3
PRB_R14	Stereo		C		Yes			5		No		32			4
PRB_R15	Stereo		C		Yes			0		25-Tap	32			4

*/
		Wire.write(decimation); // 14 or 8 (14 is actually quite bad to use!)
		Wire.endTransmission();
		
		// There needs to be a raft of sending coefficient values before bothering with the following pair of writes.
		// writeReg(AAFCR,1<<2); // ADC adaptive filtering enable
		//writeReg(DAFCR,1<<2); // DAC adaptive filtering enable
		
	// Configure power supplies and powertune
		writeReg(PCR,8); // disable internal crude AVdd in presence of external AVdd supply or before powering up internal Avdd LDO
		writeReg(LDOCR,(2<<6)|(2<<4)|1); // DVDD LDO ~1.77V, AVDD LDO ~1.77V, AVDD LDO power up
		writeReg(CMCR,(3<<4)|(1<<3)|(1<<1)|1); // CM=0.9V, HP_CM=1.65V, LO(LR)_CM=1.65V, HP from LDOIN, 1.8 < LDOIN < 3.6
		writeReg(ADCPTCR,0); // powerTune ADC = PTM_R4 - DAC powerTune defaults to my  setting.
		writeReg(AIQCCR,50); // MicPGA startup delay=6.4ms
		writeReg(RPCR,1); // REF charge up time 40ms
		
	// Configure headphone output a bits
		writeReg(HDSCR,(0<<6)|(5<<2)|1);

	// ADC routing and power
		// In1L -> DOUT_L, In1R -> DOUT_R
		writeReg(LMPPTIRCR,3<<6); // In1L-40k-MicPGA_PL     : [1,52]
		writeReg(LMPNTIRCR,3<<6); // CM(CM1L)-40k-MicPGA_NL : [1,54]
		writeReg(RMPPTIRCR,3<<6); // In1R-40k-MicPGA_R      : [1,55]
		writeReg(RMPNTIRCR,3<<6); // CM(CM1R)-40k-MicPGA_NR
		writeReg(LMPVCR,0); // no attempt to recover signal attenuated by connection
		writeReg(RMPVCR,0); 
		writeReg(ACSR,192); // power up ADC_L & ADC_R
		writeReg(AFGAR,0); // unmute ADC_L & ADC_R
		
		// floating inputs tie down...
		writeReg(FICR,(1<<5)|(1<<4)|(1<<3)|(1<<2)); // In2(L&R) and In3(L&R) weak connected to common mode.

	// DAC routing and power
		writeReg(PCR1,0); // Left DAC, in PTM_P3/PTM_P4 mode route to HPL as AB class - default
		writeReg(PCR2,0); // Right DAC, in PTM_P3/PTM_P4 mode route to HPR as AB class - default
		writeReg(LLRSR,1<<3); // Left Channel DAC reconstruction filter output routed to LOL
		writeReg(LRRSR,1<<3); // Right Channel DAC reconstruction filter output routed to LOR
		writeReg(HPLRSR,1<<3); // Left Channel DAC reconstruction filter output routed to HPL
		writeReg(HPRRSR,1<<3); // Right Channel DAC reconstruction filter output routed to HPR
		writeReg(ODPCR,(1<<3)|(1<<2)|(0<<1)|0); // LOL power up, LOR power up, Left Mixer power up, Right Mixer power up.
		writeReg(DCSR1,(1<<7)|(1<<6)|(1<<4)|(1<<2)); // DAC Left enable DAC Right enable, Left DAC from AIF, Right DAC from AIF
		writeReg(DCSR2,0); // Unmute the DAC (again!)
		
		writeReg(LCAGCCR3,12);
		writeReg(RCAGCCR3,12); // 6dB max gain in AGC
		
		writeReg(HPLVCR,116);
		writeReg(HPRVCR,116);
		
		writeReg(MALVCR,39);
		writeReg(MARVCR,39);
		
		#warning writing stuff in the right places...
		
		writeReg(MICBIASCR,0); // mic bias off, use 64 to make mic bias up, src: AVDD op: 1.25V
		return true;
	}


	bool AudioControlTLV320::enable()
	{
		curPage=255;
	// software reset
		// Serial.println("writeReg(SRR,1);");
		writeReg(SRR,1); // selected page 0, wrote '1' to register 1 to soft reset.
		// Serial.print("delay(50);");
		delay(50); // the device is resetting, wait...
		
		// MCLK,MADC,NADC,AOSR,MDAC,NDAC,DOSR,fS (PLL_CLOCKOUT),P,R,J,D
		// 24576000,8,4,32,8,4,32,96000 (98304000),4,1,16,0
		// 24576000,4,4,64,4,4,64,96000
		Wire.beginTransmission(TLV320xxx_I2C);
		Wire.write(4); // select register 4 for start of writes.
		Wire.write((1<<6)|3); // selecting MCLK as input to PLL, PLL as input to CODEC_CLKIN - High PLL range
		Wire.write((1<<7)|(4<<4)|1); // P & R values to register 5 // 1<<7 = PLL enabled, 4<<7 = P value=4, 1= R value=1
		Wire.write(16); // J value to register 6
		Wire.write(0); // MSB of D value to register 7
		Wire.write(0); // LSB of D value to register 8

		Wire.write(0); // skip register 9
		Wire.write(0); // skip register 10
		Wire.write((1<<7)|4); // NDAC value to register 11, divider powered on
		Wire.write((1<<7)|4); // MDAC value to register 12, divider powered on
		Wire.write(0); // DOSR value (MSB) to register 13
		Wire.write(64); // DOSR value (LSB) to register 14
		Wire.endTransmission(); 
		
	// I2S set up
		// writeReg(AISR1,0); // Audio Interface: I2S, 16bit, BCLK is input, WCLK is input, DOUT always driven - the default set up.

		writeReg(CSR10,3); // CDIV_CLKIN = PLL
		writeReg(CSR11,129); // M divider powered up and equ 1
		writeReg(MFP5CR,0); // CLKOUT

		
		Wire.beginTransmission(TLV320xxx_I2C);
		Wire.write(18); // select register 18 for start of next writes
		Wire.write((0<<7)|4); // NADC value to register 18, divider off
		Wire.write((1<<7)|4); // MADC value to register 19, divider on
		Wire.write(64); // AOSR value to register 20
		Wire.endTransmission();




		
	// processing blocks or miniDSP
		Wire.beginTransmission(TLV320xxx_I2C);
		Wire.write(60); // select register 60, we are on page 0
/*		
Proc	Interpolation	Channel	1st Order	Num. 	DRC	3D	Beep		Resource
Block	Filter					IIR 		Biquads			Generator	Class
--------------------------------------------------------------------------------
PRB_P7		B			Stereo	Yes			0		No	No	No			6
PRB_P8		B			Stereo	No			4		Yes	No	No			8 <-- went with this one.
PRB_P9		B			Stereo	No			4		No	No	No			8
PRB_P10		B			Stereo	Yes			6		Yes	No	No			10
PRB_P11		B			Stereo	Yes			6		No	No	No			8
PRB_P12		B			Left	Yes			0		No	No	No			3
PRB_P13		B			Left	No			4		Yes	No	No			4
PRB_P14		B			Left	No			4		No	No	No			4
PRB_P15		B			Left	Yes			6		Yes	No	No			6 <--- maybe this one is better choice?
PRB_P16		B			Left	Yes			6		No	No	No			4
*/
		Wire.write(8);
/*
Proc	Channel	Decimation	1st Order	Number	FIR		Required	Resource
Block			Filter		IIR			BiQuads			AOSR Value	Class
----------------------------------------------------------------------------
PRB_R7	Stereo		B		Yes			0		No		64			3
PRB_R8	Stereo		B		Yes			3		No		64			4 <-- using this one atm.
PRB_R9	Stereo		B		Yes			0		20-Tap	64			4
*/
		Wire.write(8);
		Wire.endTransmission();
		
		// There needs to be a raft of sending coefficient values before bothering with the following pair of writes.
		writeReg(AAFCR,1<<2); // ADC adaptive filtering enable
		//writeReg(DAFCR,1<<2); // DAC adaptive filtering enable
		
	// Configure power supplies and powertune
		writeReg(PCR,8); // disable internal crude AVdd in presence of external AVdd supply or before powering up internal Avdd LDO
		writeReg(LDOCR,(2<<6)|(2<<4)|1); // DVDD LDO ~1.77V, AVDD LDO ~1.77V, AVDD LDO power up
		writeReg(CMCR,(3<<4)|(1<<3)|(1<<1)|1); // CM=0.9V, HP_CM=1.65V, LO(LR)_CM=1.65V, HP from LDOIN, 1.8 < LDOIN < 3.6
		writeReg(ADCPTCR,0); // powerTune ADC = PTM_R4 - DAC powerTune defaults to my  setting.
		writeReg(AIQCCR,50); // MicPGA startup delay=6.4ms
		writeReg(RPCR,1); // REF charge up time 40ms
		
	// Configure headphone output a bits
		writeReg(HDSCR,37); // Rpop=6k, soft step=20usec should work with 47uF coupling

	// ADC routing and power
		// In1L -> DOUT_L, In1R -> DOUT_R
		writeReg(LMPPTIRCR,3<<6); // In1L-40k-MicPGA_PL     : [1,52]
		writeReg(LMPNTIRCR,3<<6); // CM(CM1L)-40k-MicPGA_NL : [1,54]
		writeReg(RMPPTIRCR,3<<6); // In1R-40k-MicPGA_R      : [1,55]
		writeReg(RMPNTIRCR,3<<6); // CM(CM1R)-40k-MicPGA_NR
		writeReg(LMPVCR,0); // no attempt to recover signal attenuated by connection
		writeReg(RMPVCR,0); 
		writeReg(ACSR,192); // power up ADC_L & ADC_R
		writeReg(AFGAR,0); // unmute ADC_L & ADC_R
		
		// floating inputs tie down...
		writeReg(FICR,(1<<5)|(1<<4)|(1<<3)|(1<<2)); // In2(L&R) and In3(L&R) weak connected to common mode.

	// DAC routing and power
		writeReg(PCR1,0); // Left DAC, in PTM_P3/PTM_P4 mode route to HPL as AB class - default
		writeReg(PCR2,0); // Right DAC, in PTM_P3/PTM_P4 mode route to HPR as AB class - default
		writeReg(LLRSR,1<<3); // Left Channel DAC reconstruction filter output routed to LOL
		writeReg(LRRSR,1<<3); // Right Channel DAC reconstruction filter output routed to LOR
		writeReg(HPLRSR,1<<3); // Left Channel DAC reconstruction filter output routed to HPL
		writeReg(HPRRSR,1<<3); // Right Channel DAC reconstruction filter output routed to HPR
		writeReg(ODPCR,(1<<3)|(1<<2)|(1<<1)|1); // LOL power up, LOR power up, Left Mixer power up, Right Mixer power up.
		writeReg(DCSR1,(1<<7)|(1<<6)|(1<<4)|(1<<2)); // DAC Left enable DAC Right enable, Left DAC from AIF, Right DAC from AIF
		writeReg(DCSR2,0); // Unmute the DAC (again!)
		
		writeReg(LCAGCCR3,12);
		writeReg(RCAGCCR3,12); // 6dB max gain in AGC
		
		writeReg(MICBIASCR,65); // mic bias up, src: AVDD op: 1.25V

		return true;
	}
	
	bool AudioControlTLV320::volume(float left, float right)
	{
		if(left<1/128) left=1/128; // minimum acceptable value I'm afraid.
		if(left>1.375) left=1.375; // +24dB
		left=-128+(left*128);
		int8_t nl=floor(left+0.499); // should be limited to -127 (representing -63.5dB) and 48 (+24dB)
		if(right<1/128) right=1/128;
		if(right>1.375) right=1.375;
		right=-128+(right*128);
		int8_t nr=floor(left+0.499);
		writeReg(LDCDVCR,nl);
		return (bool)writeReg(RDCDVCR,nr);
	}


	unsigned char AudioControlTLV320::dBadjustDAC(float left, float right)
	{
		left*=2;
		right*=2;
		int8_t nl=left;
		int8_t nr=right;
		if(nl<-127) nl=-127;
		if(nr>48) nl=48;
		if(nl<-127) nr=-127;
		if(nr>48) nr=48;
		writeReg(LDCDVCR,nl);
		return writeReg(RDCDVCR,nr);
	}



	void AudioControlTLV320::routeIn1(unsigned char left, unsigned char right)
	{
		left&=3;
		right&=3;
		writeReg(LMPPTIRCR,left<<6); // In1L-40k-MicPGA_PL     : [1,52]
		writeReg(LMPNTIRCR,left<<6); // CM(CM1L)-40k-MicPGA_NL : [1,54]
		writeReg(RMPPTIRCR,right<<6); // In1R-40k-MicPGA_R      : [1,55]
		writeReg(RMPNTIRCR,right<<6); // CM(CM1R)-40k-MicPGA_NR
	}
	
	void AudioControlTLV320::routeIn2(unsigned char left, unsigned char right)
	{
		left&=3;
		right&=3;
		writeReg(LMPPTIRCR,left<<4); // In1L-40k-MicPGA_PL     : [1,52]
		writeReg(LMPNTIRCR,left<<4); // CM(CM1L)-40k-MicPGA_NL : [1,54]
		writeReg(RMPPTIRCR,right<<4); // In1R-40k-MicPGA_R      : [1,55]
		writeReg(RMPNTIRCR,right<<4); // CM(CM1R)-40k-MicPGA_NR
	}
	
	void AudioControlTLV320::routeIn3(unsigned char left, unsigned char right)
	{
		left&=3;
		right&=3;
		writeReg(LMPPTIRCR,left<<2); // In1L-40k-MicPGA_PL     : [1,52]
		writeReg(LMPNTIRCR,left<<2); // CM(CM1L)-40k-MicPGA_NL : [1,54]
		writeReg(RMPPTIRCR,right<<2); // In1R-40k-MicPGA_R      : [1,55]
		writeReg(RMPNTIRCR,right<<2); // CM(CM1R)-40k-MicPGA_NR
	}

	
	
	unsigned char AudioControlTLV320::lineOutMute(bool left, bool right)
	{
		uint8_t nl=(readReg(LLDGSR)&127)|(left<<7);
		writeReg(LLDGSR,nl);
		uint8_t nr=(readReg(LRDGSR)&127)|(right<<7);
		return writeReg(LRDGSR,nr);
	}
	
	unsigned char AudioControlTLV320::headPhoneMute(bool left, bool right)
	{
		uint8_t nl=(readReg(HPLDGSR)&127)|(left<<7);
		writeReg(HPLDGSR,nl);
		uint8_t nr=(readReg(HPRDGSR)&127)|(right<<7);
		return writeReg(HPRDGSR,nr);
		
	}
	
	
	unsigned char AudioControlTLV320::lineOutEn(bool nl, bool nr)
	{
		uint8_t m=readReg(ODPCR)&~(3<<2);
		m|=((nl<<3)|(nr<<2));
		return writeReg(ODPCR,m);
	}
	
	unsigned char AudioControlTLV320::dacOutEn(bool nl, bool nr)
	{
		uint8_t m=(readReg(DCSR1)&~192)|(nl<<7)|(nr<<6);
		return writeReg(DCSR1,m);
	}
	

	
	unsigned char AudioControlTLV320::headPhoneEn(bool nl, bool nr)
	{
		uint8_t m=readReg(ODPCR)&~(3<<4);
		m|=((nl<<5)|(nr<<4));
		return writeReg(ODPCR,m);
	}
	
	unsigned char AudioControlTLV320::dBlineInAdjust(float left, float right)
	{
		s8convertu8 nl,nr;
		nl.ns=left*2;
		nr.ns=right*2;
		if(nl.ns<-24) nl.ns=-24;
		if(nl.ns>40) nl.ns=40;
		if(nr.ns<-24) nr.ns=-24;
		if(nr.ns>40) nr.ns=40;
		
		if(nl.nu&128) {
			nl.nu&=127; // clear B7
			nl.nu|=64; // set B6 - sign bit is not B7 but B6 in this register.
		}
		if(nr.nu&128) {
			nr.nu&=127;
			nr.nu|=64;
		}
		
		writeReg(LACVCR,nl.nu);
		return writeReg(RACVCR,nr.nu);
	}
	
	unsigned char AudioControlTLV320::dBlineOutAdjust(float left, float right)
	{
		s8convertu8 nl,nr;
		
		nl.ns=left;
		nr.ns=right;
		if(nl.ns<-6) nl.ns=-6;
		if(nl.ns>29) nl.ns=29;
		if(nr.ns<-6) nr.ns=-6;
		if(nr.ns>29) nr.ns=29;
		
		if(nl.nu&128) {
			nl.nu&=~192; // clear B7
			nl.nu|=32; // set B5 - sign bit is not B7 but B5 in this register.
		}
		if(nr.nu&128) {
			nr.nu&=~192;
			nr.nu|=32;
		}
		
		writeReg(LLDGSR,nl.nu);
		return writeReg(LRDGSR,nr.nu);
	}

	unsigned char AudioControlTLV320::dBheadPhoneAdjust(float left, float right)
	{
		s8convertu8 nl,nr;
		
		nl.ns=left;
		nr.ns=right;
		if(nl.ns<-24) nl.ns=-6;
		if(nl.ns>29) nl.ns=29;
		if(nr.ns<-24) nr.ns=-6;
		if(nr.ns>29) nr.ns=29;
		
		if(nl.nu&128) {
			nl.nu&=127; // clear B7
			nl.nu|=32; // set B5 - sign bit is not B7 but B5 in this register.
		}
		if(nr.nu&128) {
			nr.nu&=127;
			nr.nu|=32;
		}
		
		writeReg(HPLDGSR,nl.nu);
		return writeReg(HPRDGSR,nr.nu);
	}
	
	unsigned char AudioControlTLV320::dBmixerGain(float left, float right)
	{
		if(left<0) left=0;
		if(left>47.5) left=47.5;
		uint8_t nl=left*2;
		if(right<0) right=0;
		if(right>47.5) right=47.5;
		uint8_t nr=right*2;
		writeReg(LMPVCR,nl);
		return writeReg(RMPVCR,nr);
	}

	unsigned char AudioControlTLV320::AGCenable(bool nl, bool nr)
	{
		uint8_t nl1=(readReg(LCAGCCR1)&~128)|(nl<<7);
		writeReg(LCAGCCR1,nl1);
		uint8_t nr1=(readReg(RCAGCCR1)&~128)|(nr<<7);
		return writeReg(RCAGCCR1,nr1);
	}
	
	bool AudioControlTLV320::AGCenabled(uint8_t channel)
	{
		if(channel) return ((readReg(RCAGCCR1)&128)==128); else return ((readReg(LCAGCCR1)&128)==128);
	}
	unsigned char AudioControlTLV320::AGCenabled(uint8_t channel, bool setting)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR1)&~128)|(setting<<7);
			return writeReg(RCAGCCR1,n);
		} else {
			uint8_t n=(readReg(LCAGCCR1)&~128)|(setting<<7);
			return writeReg(LCAGCCR1,n);
		}
	}
	
	unsigned char AudioControlTLV320::AGCdBtarget(uint8_t channel) // returns the current target in the given channel.
	{
		if(channel) return (readReg(RCAGCCR1)&(15<<4))>>4;
		return (readReg(LCAGCCR1)&(7<<4))>>4;
	}
	unsigned char AudioControlTLV320::AGCdBtarget(uint8_t channel, uint8_t target) // sets the current target
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR1)&~(7<<4))|((target&7)<<4);
			return writeReg(RCAGCCR1,n);
		} else {
			uint8_t n=(readReg(LCAGCCR1)&~(7<<4))|((target&7)<<4);
			return writeReg(LCAGCCR1,n);
		}
	}
	
	unsigned char AudioControlTLV320::AGCdBgainHysteresis(uint8_t channel)
	{
		if(channel) return readReg(RCAGCCR1)&3;  else return readReg(LCAGCCR1)&3;
	}
	unsigned char AudioControlTLV320::AGCdBgainHysteresis(uint8_t channel, uint8_t gainHyst)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR1)&~3)|(gainHyst&3);
			return writeReg(RCAGCCR1,n);
		} else {
			uint8_t n=(readReg(LCAGCCR1)&~3)|(gainHyst&3);
			return writeReg(LCAGCCR1,n);
		}
	}
	
	unsigned char AudioControlTLV320::AGCdBhysteresis(uint8_t channel)
	{
		if(channel) return (readReg(RCAGCCR2)&192)>>6; else return (readReg(LCAGCCR2)&192)>>6;
	}
	
	unsigned char AudioControlTLV320::AGCdBhysteresis(uint8_t channel, uint8_t hyst)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR2)&~192)|((hyst&3)<<6);
			return writeReg(RCAGCCR2,n);
		} else {
			uint8_t n=(readReg(LCAGCCR2)&~192)|((hyst&3)<<6);
			return writeReg(LCAGCCR2,n);
		}
	}
	
	unsigned char AudioControlTLV320::AGCdBnoiseThreshold(uint8_t channel)
	{
		if(channel) return (readReg(RCAGCCR2)&62)>>1; else return (readReg(LCAGCCR2)&62)>>1;
	}
	
	unsigned char AudioControlTLV320::AGCdBnoiseThreshold(uint8_t channel, uint8_t thresh)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR2)&~62)|((thresh&31)<<1);
			return writeReg(RCAGCCR2,n);
		} else {
			uint8_t n=(readReg(LCAGCCR2)&~62)|((thresh&31)<<1);
			return writeReg(LCAGCCR2,n);
		}
	}
	
	unsigned char AudioControlTLV320::AGCdBmaxGain(uint8_t channel)
	{
		if(channel) return readReg(RCAGCCR3)&127; else return readReg(LCAGCCR3)&127;
	}
	
	unsigned char AudioControlTLV320::AGCdBmaxGain(uint8_t channel, uint8_t gain)
	{
		if(gain>250) gain=0;
		if(gain>116) gain=116;
		if(channel) return writeReg(RCAGCCR3,gain); else return writeReg(LCAGCCR3,gain);
	}
	
	unsigned char AudioControlTLV320::AGCattackSetting(uint8_t channel)
	{
		if(channel) return (readReg(RCAGCCR4)&248)>>3; else return (readReg(LCAGCCR4)&248)>>3;
	}
	unsigned char AudioControlTLV320::AGCattackSetting(uint8_t channel, uint8_t setting)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR4)&~248)|((setting&31)<<3);
			return writeReg(RCAGCCR4,n);
		} else {
			uint8_t n=(readReg(LCAGCCR4)&~248)|((setting&31)<<3);
			return writeReg(LCAGCCR4,n);
		}
	}
	
	unsigned char AudioControlTLV320::AGCattackScale(uint8_t channel)
	{
		if(channel) return readReg(RCAGCCR4)&7; else return readReg(LCAGCCR4)&7;
	}
	unsigned char AudioControlTLV320::AGCattackScale(uint8_t channel, uint8_t setting)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR4)&~7)|(setting&7);
			return writeReg(RCAGCCR4,n);
		} else {
			uint8_t n=(readReg(LCAGCCR4)&~7)|(setting&7);
			return writeReg(LCAGCCR4,n);
		}
	}

	unsigned char AudioControlTLV320::AGCdecaySetting(uint8_t channel)
	{
		if(channel) return (readReg(RCAGCCR5)&248)>>3; else return (readReg(LCAGCCR5)&248)>>3;
	}
	unsigned char AudioControlTLV320::AGCdecaySetting(uint8_t channel, uint8_t setting)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR5)&~248)|((setting&31)<<3);
			return writeReg(RCAGCCR5,n);
		} else {
			uint8_t n=(readReg(LCAGCCR5)&~248)|((setting&31)<<3);
			return writeReg(LCAGCCR5,n);
		}
		
	}
	
	unsigned char AudioControlTLV320::AGCdecayScale(uint8_t channel)
	{
		if(channel) return readReg(RCAGCCR5)&7; else return readReg(LCAGCCR5)&7;
		
	}
	unsigned char AudioControlTLV320::AGCdecayScale(uint8_t channel, uint8_t setting)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR5)&~7)|(setting&7);
			return writeReg(RCAGCCR5,n);
		} else {
			uint8_t n=(readReg(LCAGCCR5)&~7)|(setting&7);
			return writeReg(LCAGCCR5,n);
		}
	}
	
	unsigned char AudioControlTLV320::AGCnoiseDebounce(uint8_t channel)
	{
		if(channel) return readReg(RCAGCCR6)&31; else return readReg(LCAGCCR6)&31;
	}
	unsigned char AudioControlTLV320::AGCnoiseDebounce(uint8_t channel, uint8_t setting)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR6)&~31)|(setting&31);
			return writeReg(RCAGCCR6,n);
		} else {
			uint8_t n=(readReg(LCAGCCR6)&~31)|(setting&31);
			return writeReg(LCAGCCR6,n);
		}
	}
	
	unsigned char AudioControlTLV320::AGCsignalDebounce(uint8_t channel)
	{
		if(channel) return readReg(RCAGCCR7)&15; else return readReg(LCAGCCR7)&15;
	}
	unsigned char AudioControlTLV320::AGCsignalDebounce(uint8_t channel, uint8_t setting)
	{
		if(channel)
		{
			uint8_t n=(readReg(RCAGCCR7)&~15)|(setting&15);
			return writeReg(RCAGCCR7,n);
		} else {
			uint8_t n=(readReg(LCAGCCR7)&~15)|(setting&15);
			return writeReg(LCAGCCR7,n);
		}
	}
	


	int8_t AudioControlTLV320::AGCcurrentGain(uint8_t channel)
	{
		if(channel) return readReg(RCAGCCR8); else return readReg(LCAGCCR8);
	}
	
	bool AudioControlTLV320::DRCenabled(uint8_t channel)
	{
		if(channel) return ((readReg(DRCCR1)&32)==32); else return ((readReg(DRCCR1)&64)==64);
	}


	unsigned char AudioControlTLV320::DRCenabled(uint8_t channel, bool setting)
	{
		if(channel)
		{
			uint8_t n=(readReg(DRCCR1)&~32)|(setting<<5);
			return writeReg(DRCCR1,n);
		} else {
			uint8_t n=(readReg(DRCCR1)&~64)|(setting<<6);
			return writeReg(DRCCR1,n);
		}
	}

	unsigned char AudioControlTLV320::DRCthresholdControl()
	{
		return ((readReg(DRCCR1)&28)>>2);
	}
	unsigned char AudioControlTLV320::DRCthresholdControl(uint8_t setting)
	{
		uint8_t n=(readReg(DRCCR1)&~28)|((setting&7)<<2);
		return writeReg(DRCCR1,n);
	}
	unsigned char AudioControlTLV320::DRChysteresisControl()
	{
		return readReg(DRCCR1)&3;
	}
	unsigned char AudioControlTLV320::DRChysteresisControl(uint8_t setting)
	{
		uint8_t n=(readReg(DRCCR1)&~3)|(setting&3);
		return writeReg(DRCCR1,n);
	}
	
	unsigned char AudioControlTLV320::DRChold()
	{
		return (readReg(DRCCR2)&120)>>3;
	}
	unsigned char AudioControlTLV320::DRChold(uint8_t setting)
	{
		uint8_t n=(readReg(DRCCR2)&~120)|(setting&15);
		return writeReg(DRCCR2,n);
	}
	
	unsigned char AudioControlTLV320::DRCattackRate()
	{
		return (readReg(DRCCR3)&240)>>4;
	}
	unsigned char AudioControlTLV320::DRCattackRate(uint8_t setting)
	{
		uint8_t n=(readReg(DRCCR3)&~240)|(setting&15)<<4;
		return writeReg(DRCCR3,n);
	}
	
	unsigned char AudioControlTLV320::DRCdecayRate()
	{
		return (readReg(DRCCR3)&15);
	}
	unsigned char AudioControlTLV320::DRCdecayRate(uint8_t setting)
	{
		uint8_t n=(readReg(DRCCR3)&~15)|(setting&15);
		return writeReg(DRCCR3,n);
	}
	

	
	
	
	void AudioControlTLV320::biquadCopyADC(unsigned char channel, unsigned char set)
	{
		// One group is 'on' while the other group is 'off' - need to copy 'on' ones to 'off' for this set...
		// Group A starts on page 8, Group B starts on page 26
		unsigned char P8_R1=readReg(AAFCR);
		// if (P8_R1&(1<<1)) ADC_USING_Buffer_B else ADC_USING_Buffer_A
		unsigned char src_P=8+channel; // starting page 8 for channel=left(0), page 9 for channel=right(1)
		unsigned char wrt_R=36+(channel*8)+(set*20); // starting register 36 for channel=left(0), register 44 for channel=right(1)
		while(wrt_R>127)
		{
			src_P++;
			wrt_R-=128;
		}
		unsigned char dst_P=src_P;
		if (P8_R1&(1<<1)) src_P+=18; else dst_P+=18;
		
		unsigned char tmp=0;
		for(unsigned char i=0;i<5;i++)
		{
			for (unsigned char j=0;j<3;j++)
			{
				tmp=readReg(src_P,wrt_R);
				writeReg(dst_P,wrt_R++,tmp);
			}
			wrt_R++;
		}
	}

	void AudioControlTLV320::biquadADC(unsigned char channel, unsigned char set, const int coefs[])
	{
		
		// reducing complication means hiding the fact that there are two copies and only one copy is in play at any given time.
		
		if(set>2) return;
		
		for(unsigned char sets=0;sets<3;sets++) {
			if(sets!=set) biquadCopyADC(channel,sets);
		}
		
		unsigned char P8_R1=readReg(AAFCR);
		// if (P8_R1&(1<<1)) ADC_USING_Buffer_B else ADC_USING_Buffer_A
		unsigned char dst_P=8+channel; // starting page 8 for channel=left(0), page 9 for channel=right(1)
		unsigned char wrt_R=36+(channel*8)+(set*20); // starting register 36 for channel=left(0), register 44 for channel=right(1)
		while(wrt_R>127)
		{
			dst_P++;
			wrt_R-=128;
		}
		if (!P8_R1&(1<<1)) dst_P+=18;
		
		uconvert32to8s tmp;
		
		for(unsigned char i=0;i<5;i++)
		{
			tmp.value=coefs[i];
			writeReg(dst_P,wrt_R++,tmp.d[2]);
			writeReg(dst_P,wrt_R++,tmp.d[1]);
			writeReg(dst_P,wrt_R++,tmp.d[0]);
			wrt_R++;
		}
		biquadSwitchADC();
	}
	
	void AudioControlTLV320::biquadCopyDAC(unsigned char channel, unsigned char set)
	{
		// One group is 'on' while the other group is 'off' - need to copy 'on' ones to 'off' for this set...
		// Group A starts on page 8, Group B starts on page 26
		unsigned char P44_R1=readReg(DAFCR);
		// if (P44_R1&(1<<1)) DAC_USING_Buffer_B else DAC_USING_Buffer_A
		unsigned char src_P=44+channel; // starting page 44 for channel=left(0), page 45 for channel=right(1)
		unsigned char wrt_R=12+(channel*8)+(set*20); // starting register 12 for channel=left(0), register 20 for channel=right(1)
		while(wrt_R>127)
		{
			src_P++;
			wrt_R-=128;
		}
		unsigned char dst_P=src_P;
		if (P44_R1&(1<<1)) src_P+=18; else dst_P+=18;
		
		unsigned char tmp=0;
		for(unsigned char i=0;i<5;i++)
		{
			for (unsigned char j=0;j<3;j++)
			{
				tmp=readReg(src_P,wrt_R);
				writeReg(dst_P,wrt_R++,tmp);
			}
			wrt_R++;
		}
	}
	
	void AudioControlTLV320::biquadDAC(unsigned char channel, unsigned char set, const int coefs[])
	{
		
		if(set>5) return;
		
		for(unsigned char sets=0;sets<6;sets++) {
			if(sets!=set) biquadCopyDAC(channel,sets);
		}
		
		unsigned char P44_R1=readReg(DAFCR);
		unsigned char dst_P=44+channel; // starting page 44 for channel=left(0), page 45 for channel=right(1)
		unsigned char wrt_R=12+(channel*8)+(set*20); // starting register 12 for channel=left(0), register 20 for channel=right(1)
		while(wrt_R>127)
		{
			dst_P++;
			wrt_R-=128;
		}
		if (!P44_R1&(1<<1)) dst_P+=18;
		
		uconvert32to8s tmp;
		
		for(unsigned char i=0;i<5;i++)
		{
			tmp.value=coefs[i];
			writeReg(dst_P,wrt_R++,tmp.d[2]);
			writeReg(dst_P,wrt_R++,tmp.d[1]);
			writeReg(dst_P,wrt_R++,tmp.d[0]);
			wrt_R++;
		}
	}
	void AudioControlTLV320::biquadSwitchADC(void)
	{
		writeReg(AAFCR,(1<<2)|1);
	}
	void AudioControlTLV320::biquadSwitchDAC(void)
	{
		writeReg(DAFCR,(1<<2)|1);
	}

/*	unsigned char AudioControlTLV320::calcVol(float n, unsigned char range)
	{
		n=(n*(((float)range)/100))+0.499;
		if ((unsigned char)n>range) n=range;
		return (unsigned char)n;
	}
*/











bool setI2SmasterClock(uint32_t freq, uint32_t mult)
{
        // determine fractional ratio for: p / q = freq * mult / f_pll
        // f_pll = 16 MHz * ((MCG_C6 & 0x1f) + 24) / ((MCG_C5 & 0x1f) + 1)
	uint32_t p = freq * mult * ((MCG_C5 & 0x1f) + 1);
        uint32_t q = 16000000ul * ((MCG_C6 & 0x1f) + 24);
	uint32_t fract = 0, divide = 1; // I2Sx_MDR register values
	uint32_t fract1, fract2 = 1;	// values of iterations n-1 and n-2.
	uint32_t divide1, divide2 = 0;	// idem.
        uint8_t src = 3;		// clock source selection: 3 = pll

        // Master clock should be slower than reference clock
        if (p > q)
                return false;
        else if (p == q)
                fract = divide = 1;
        else {
	        // Approximate p/q by a continued fraction expansion.
  	        // (typically takes only 4 to 8 iterations)
  	        while (p) {
  	    		// find next element of continued fraction
	  		uint32_t a = q / p;
		  	uint32_t oldp = p;
  			p = q - a * p;
	  		q = oldp;

    		  	// update fract/divide by Wallis method
  			fract1 = fract;
	  		divide1 = divide;
		  	fract = fract1 * a + fract2;
  			divide = divide1 * a + divide2;
	  		fract2 = fract1;
		  	divide2 = divide1;

  			// Exit loop if fraction gets too big for the registers
	  		if (fract > 256 || divide > 4096) {
		  		// Revert back to last approximation that still fitted
  				fract = fract2;
	  			divide = divide2;
		  		break;
  			}
	  	}
  	        // Check if frequency too low or too high
          	if (!fract)
                  	return false;
        }

	// Actually set registers and enable MCLK.
        SIM_SCGC6 |= SIM_SCGC6_I2S;
	I2S0_MDR = I2S_MDR_FRACT((fract - 1)) | I2S_MDR_DIVIDE((divide - 1));
        I2S0_MCR = I2S_MCR_MICS(src) | I2S_MCR_MOE;

	// 'Connect' MCLK output to an actual output pin
        CORE_PIN11_CONFIG = PORT_PCR_MUX(6);
        //CORE_PIN28_CONFIG = PORT_PCR_MUX(4); // Pin on bottom

        return true;
}





