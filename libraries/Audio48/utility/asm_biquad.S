




.cpu cortex-m4
.syntax unified
.thumb
.text
.align	2



/* The following is more or less a test, it 'blends' in well enough with the existing
   software to allow testing without having to complete any of the new software
   
   TESTED, filtering result equal to well-written-cpp; reduced Audio Library Processor usage from 42% to 32% */

/* void gasmBiquadifi(int16_t* audio_data16, float* inter_buffer, float* coefs); */
 .global	gasmBiquadifi
.thumb_func
	gasmBiquadifi:

	@ r0: audio_data16 - or, so I am told.
	@ r1: inter_buffer - Because these are *NOT*
	@ r2: coefs - instructions, I wish they were :/

	push {r4-r8}
	vpush {s0-s10}
	
	
/*	s0 = b0;
	s1 = b1;
	s2 = b2;
	s3 = a1;
	s4 = a2;
	s5 = aprev1;
	s6 = aprev2;
	s7 = bprev1;
	s8 = bprev2;
	
	s9 = sum;
	s10 = sample;

	r3 = audio_data16_rst
	r4 = inter_buffer_rst
	r5 = prevs
	r6 = audio_data16_eop
	r7 = temp
	r8 = inter_buffer_eop

	*/
	
	add r6,r0,#256 // audio_data16_eop=audio_data16+128;

	vldmia.32 r2!, {s0,s1,s2,s3,s4} // b0=*coefs++; b1=*coefs++; b2=*coefs++; a1=*coefs++; a2=*coefs++;
	mov r5,r2 // prevs=coefs;
	vldmia.32 r2!, {s5,s6,s7,s8,s9} // aprev1=*coefs++; aprev2=*coefs++; bprev1=*coefs++; bprev2=*coefs++; sum=*coefs++;
	
	vcmp.f32 s9,#0 // vf=compare(sum,0);
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!)
	beq .loopintonly // if (f.equal) goto loopintonly;

	mov r3,r0 // audio_data16_rst=audio_data16;
	mov r4,r1 // inter_buffer_rst=inter_buffer;
	add r8,r1,#512 // inter_buffer_eop=inter_buffer+128;

//	.align 2
.loopint2float:
	ldrsh r7,[r0],#2 // temp=(int32_t)*audio_data16++;
	vmov s10,r7 // (!!int32_t)sample=temp;
	vcvt.f32.s32 s10,s10 // sample=(float)sample;   // try vcvt.what-I-want.what-I-have %0,%1
	vmul.f32 s9,s10,s0 // sum=sample*b0;
	vfma.f32 s9,s1,s7 // sum+=b1*bprev1;
	vfma.f32 s9,s2,s8 // sum+=b2*bprev2
	vfma.f32 s9,s3,s5 // sum+=a1*aprev1;
	vfma.f32 s9,s4,s6 // sum+=a2*aprev2;
	vmov.f32 s6,s5 // aprev2=aprev1;
	vmov.f32 s5,s9 // aprev1=sum;
	vmov.f32 s8,s7 // bprev2=bprev1;
	vmov.f32 s7,s10 // bprev1=sample;
	vstmia.32 r1!, {s5} // *inter_buffer++=aprev1;
	cmp r0,r6 // f=compare(audio_data16_eop,audio_data16);
	blt .loopint2float // if(f.less_than) goto loopint2float

//	.align 2
.loopnextone:
	vstmia.32 r5, {s5,s6,s7,s8} // *prevs++=aprev1; *prevs++=aprev2; *prevs++=bprev1; *prevs++=bprev2;
	vldmia.32 r2!, {s0,s1,s2,s3,s4} // b0=*coefs++; b1=*coefs++; b2=*coefs++; a1=*coefs++; a2=*coefs++;
	mov r5,r2 // prevs=coefs;
	vldmia.32 r2!, {s5,s6,s7,s8,s9} // aprev1=*coefs++; aprev2=*coefs++; bprev1=*coefs++; bprev2=*coefs++; sum=*coefs++;
	mov r1,r4 // inter_buffer=inter_buffer_rst;
	vcmp.f32 s9,#0 // vf=compare(sum,0);
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!)
	beq .startfloat2int // if (f.equal) goto startfloat2int;

//	.align 2
.loopallfloat:
	vldr.f32 s10,[r1] // sample=*inter_buffer;
	vmul.f32 s9,s10,s0 // sum=sample*b0;
	vfma.f32 s9,s1,s7 // sum+=b1*bprev1;
	vfma.f32 s9,s3,s5 // sum+=a1*aprev1;
	vfma.f32 s9,s4,s6 // sum+=a2*aprev2;
	vfma.f32 s9,s2,s8 // sum+=b2*bprev2
	vmov.f32 s6,s5 // aprev2=aprev1;
	vmov.f32 s5,s9 // aprev1=sum;
	vmov.f32 s8,s7 // bprev2=bprev1;
	vmov.f32 s7,s10 // bprev1=sample;
	vstmia.32 r1!, {s5} // *inter_buffer++=aprev1;
	cmp r1,r8 // f=compare(inter_buffer_eop,inter_buffer);
	blt .loopallfloat // if(f.less_than) goto loopallfloat

	b .loopnextone // goto loopnextone
	
//	.align 2
.startfloat2int:
	mov r0,r3 // audio_buffer16=audio_buffer16_rst;

//	.align 2
.loopfloat2int:
	vldmia.32 r1!,{s10} // sample=*inter_buffer++;
	vmul.f32 s9,s10,s0 // sum=sample*b0;
	vfma.f32 s9,s1,s7 // sum+=b1*bprev1;
	vfma.f32 s9,s3,s5 // sum+=a1*aprev1;
	vfma.f32 s9,s4,s6 // sum+=a2*aprev2;
	vfma.f32 s9,s2,s8 // sum+=b2*bprev2
	vmov.f32 s6,s5 // aprev2=aprev1;
	vmov.f32 s5,s9 // aprev1=sum;
	vmov.f32 s8,s7 // bprev2=bprev1;
	vmov.f32 s7,s10 // bprev1=sample;
	vcvt.s32.f32 s10,s5 // (!!int32_t)sample=((int32_t)aprev1);
	vmov r7,s10 // temp=(int32_t)sample;
	ssat r7,16,r7 // temp=signed_saturate(temp);
	strh r7,[r0],#2 // *audio_data16++=(int16_t)temp;
	cmp r0,r6 // f=compare(audio_data16,audio_data16_eop);
	blt .loopfloat2int // if(f.less_than) goto loopfloat2int

	b .exitviahere // goto exitviahere

//	.align 2
.loopintonly:
	ldrsh r7,[r0] // temp=(int32_t)*audio_data16;
	vmov s10,r7 // (!!int32_t)sample=temp;
	vcvt.f32.s32 s10,s10 // sample=(float)sample;
	vmul.f32 s9,s10,s0 // sum=sample*b0;
	vfma.f32 s9,s1,s7 // sum+=b1*bprev1;
	vfma.f32 s9,s3,s5 // sum+=a1*aprev1;
	vfma.f32 s9,s4,s6 // sum+=a2*aprev2;
	vfma.f32 s9,s2,s8 // sum+=b2*bprev2
	vmov.f32 s6,s5 // aprev2=aprev1;
	vmov.f32 s5,s9 // aprev1=sum;
	vmov.f32 s8,s7 // bprev2=bprev1;
	vmov.f32 s7,s10 // bprev1=sample;
	vcvt.s32.f32 s10,s5 // (!!int32_t)sample=((int32_t)aprev1);
	vmov r7,s10 // temp=(int32_t)sample;
	ssat r7,16,r7 // temp=signed_saturate16(temp);
	strh r7,[r0],#2 // *audio_data16++=(int16_t)temp;
	cmp r0,r6 // f=compare(audio_data16,audio_data16_eop);
	blt .loopintonly // if(f.less_than) goto loopintonly

//	.align 2
.exitviahere:
	vstmia.32 r5, {s5,s6,s7,s8} // *prevs++=aprev1; *prevs++=aprev2; *prevs++=bprev1; *prevs++=bprev2;
	vpop {s0-s10}
	pop	{r4-r8}
	bx lr
	

	
#if 1	

/* Experiment regarding returning values to caller - is it meant to be in r4?

   UNTESTED */

/* float gasmAbsf(float num); */
 .global	gasmAbsf
.thumb_func
	gasmAbs:

	@ r0: num
	
	vpush {s0}
	
	vmov s0,r0 // contents of r0 should already be formatted like a float.
	vabs.f32 s0,s0
	vmov r4,s0 // pity, I was hoping for a single instruction, maybe two tops.

	vpop {s0}
	bx lr
	



/* All the ASM needed for a new Audio object which does all processing required for Eicon-VIC Preamps.

	48000/128 = 375 blocks of data to process per second
	12000000/375 = 320000 cycles to process each block (and run everything else, actually, good if we halve this or better)

integer to float  with peak checking (3 channels)
	<1430 to convert and follow peak in floats
	3*1430 = 4290, 4290/320000=0.01340625, 1.134% processor time doing conversion to float

filtering   
	below I calculate cycles<2000 to apply one set of coefficients as a biquad to a block of samples in pure floating point.

	5 + 5 + 5 + 1 + 3 + 4 + 2 = 25 - pretty sure I am going to use 25 sets of coefficients.
	25*2000 = 50000, 50000/320000=0.15625, 15.625% processor time doing filters - cool if this works out as correct.
   
mixing 2 channels with gain control each
	<1300 to do blend mixing with gain application
	2*1300 = 2600, 2600/320000=0.008125, 0.8125% processor time doing mix with gain
	
	
float to integer  with floating gain control (2 channels)
	<1100 to convert after applying gain
	2*1100 = 2200, 2200/320000=0.006875, 0.6875% processor time

My entire Audio Pathway is described and, apparently, should take less than 20% of processor time to run - not including i2s objects.

	UNTESTED */
	
	
/* void gasmInt2Float(int16_t* ptr16, float* ptrf); */
 .global	gasmInt2Float
.thumb_func
	gasmInt2Float:

	@ r0: ptr16
	@ r1: ptrf
	vpush {s0}

	add r2,r0,#256 // ptr16_eop=ptr16+128;
.loopgi2f:
	ldrsh r3,[r0],#2 // int32=*ptr16++
	vmov s0,r3 // float=int32
	vcvt.f32.s32 s0,s0 // vcvt.what-I-want.what-I-have
	vstmia.32 r1!, {s0} // *ptrf++=float;
	cmp r0,r2
	blt .loopgi2f

	vpop {s0}
	bx lr
	
/* void gasmInt2FloatPeak(int16_t* ptr16, float* ptrf, float* peaks); */
 .global	gasmInt2FloatPeak
.thumb_func
	gasmInt2FloatPeak:

	@ r0: ptr16
	@ r1: ptrf
	@ this is a fuckin comment fs. r2: peaks
	
	vpush {s0-s2}
	
	mov r3,r2
	add r2,r0,#256 // ptr16_eop=ptr16+128; 
	vldmia r3, {s1,s2} // peaklo=peaks[0]; peakhi=peaks[1];
.loopgi2fp: // 128*11(+/-1) =~ 1408 - probable overhead ~20 - ~1430 cycles per
	ldrsh r3,[r0],#2 // int32=*ptr16++  // 1
	vmov s0,r3 // int32=int32           // 1
	vcvt.f32.s32 s0,s0 // float=int32   // 1
	vcmp.f32 s1,s0 // f=compare(peaklo,float); // 1
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!)
	IT LT
	vmovlt s1,s0 // if(f.less_than) peaklo=float; // 1
	vcmp.f32 s2,s0 // f=compare(peakhi,float);  // 1
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!)
	IT GT
	vmovgt s2,s0 // if(f.greater_than) peakhi=float; // 1
	vstmia.32 r1!, {s0} // *ptrf++=float; // 2
	cmp r0,r2                             // 1
	blt .loopgi2fp                         // 1

	vstmia r3, {s1,s2} // peaks[0]=peaklo; peaks[1]=peakhi;

	vpop {s0-s2}
	bx lr
	
/* void gasmFloat2Int(float* ptrf, int16_t* ptr16); */
 .global	gasmFloat2Int
.thumb_func
	gasmFloat2Int:

	@ r0: ptrf
	@ r1: ptr16

	vpush {s0}
	add r2,r1,#256 // ptr16_eop=ptr16+128;
.loopgf2i:
	vldmia.32 r0!, {s0} // float=*ptrf++;
	vcvt.s32.f32 s0,s0 // float=(int32_t)float;
	vmov r3,s0 // temp=float;
	ssat r3,16,r3 // temp=signed_saturate(temp);
	strh r3,[r1],#2 // *ptr16++=temp;
	cmp r1,r2 // f=compare(r0,r2);
	blt .loopgf2i // if(f.less_than) goto loopgf2i;
	vpop {s0}
	bx lr

/* void gasmFloat2IntGain(float* ptrf, int16_t* ptr16, float* gain); */
 .global	gasmFloat2IntGain
.thumb_func
	gasmFloat2IntGain:

	@ r0: ptrf
	@ r1: ptr16
	@ r2: gain

	vpush {s0-s1}

	vldmia.32 r2, {s1} // gain0=*gain;
	add r2,r1,#256 // ptr16_eop=ptr16+128;
.loopgf2ig: // 128*8(+/-1) = ~1024, gotta be less than 1100 all up here.
	vldmia.32 r0!, {s0} // float=*ptrf++;
	vmul.f32 s0,s0,s1 // float=float*gain;
	vcvt.s32.f32 s0,s0 // (int32_t*)&float=(int32_t)float;
	vmov r3,s0 // temp=float;
	ssat r3,16,r3 // temp=signed_saturate(temp);
	strh r3,[r1],#2 // *ptr16++=temp;
	cmp r0,r2 // f=compare(r0,r2);
	blt .loopgf2ig // if(f.less_than) goto loopgf2i;

	vpop {s0-s1}
	bx lr
	
/* void gasmFloat2IntGainPeak(float* ptrf, int16_t* ptr16, float* gain, float* peak); */
 .global	gasmFloat2IntGainPeak
.thumb_func
	gasmFloat2IntGainPeak:

	@ r0: ptrf
	@ r1: ptr16
	@ r2: gain
	@ r3: peak
	
	push {r4}
	vpush {s0-s1}

	vldmia.32 r3, {s1,s2} // peaklo=peak[0]; peakhi=peak[1];
	vldmia.32 r2, {s3} // gain0=*gain
	
	add r2,r1,#256 // ptr16_eop=ptr16+128; [ Reassigned r2 because gain0 will not be returned ]
loopgf2igp: // 128*8(+/-1) = ~1024, gotta be less than 1100 all up here.
	vldmia.32 r0!, {s0} // float=*ptrf++;

	vcmp.f32 s1,s0 // f=compare(peaklo,float); // 1
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!)
	IT LT
	vmovlt s1,s0 // if(f.less_than) peaklo=float; // 1
	vcmp.f32 s2,s0 // f=compare(peakhi,float);  // 1
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!)
	IT GT
	vmovgt s2,s0 // if(f.greater_than) peakhi=float; // 1
	
	
	vmul.f32 s0,s0,s3 // float=float*gain0;
	vcvt.s32.f32 s0,s0 // (int32_t*)&float=(int32_t)float;
	vmov r4,s0 // temp=float;
	ssat r4,16,r4 // temp=signed_saturate(temp);
	strh r4,[r1],#2 // *ptr16++=temp;
	cmp r0,r2 // f=compare(r0,r2);
	blt loopgf2igp // if(f.less_than) goto loopgf2ip;

	vstmia r3, {s1,s2} // peak[0]=peaklo; peak[1]=peakhi;
	
	vpop {s0-s1}
	pop {r4}
	bx lr
	

/* void gasmBiquadf(float* ptrf, float* coefs); */
 .global gasmBiquadf
.thumb_func
	gasmBiquadf:
	
	@ r0: ptrf
	@ r1: coefs
	
	push {r4}
	vpush {s0-s11}
	
	add r3,r0,#512 // ptrf_eop=ptrf+128;
gasmbqfloopmain: // number of coefficient sets * (21 + gasmbqloopinner.cycles)
	vldmia.32 r1!, {s0,s1,s2,s3,s4} // b0=*coefs++; b1=*coefs++; b2=*coefs++; a1=*coefs++; a2=*coefs++;                    // 6 cycles
	mov r4,r1 // prevs=coefs;                                                                                              // 1 cycle
	vldmia.32 r1!, {s5,s6,s7,s8,s9} // aprev1=*coefs++; aprev2=*coefs++; bprev1=*coefs++; bprev2=*coefs++; temp1=*coefs++; // 6 cycles
	mov r2,r0 // ptrf_use=ptrf;                                                                                            // 1 cycle
gasmbqfloopinner: // 15(+/-1) cycles * 128 =~ 1920 cycles
	vldr.f32 s10,[r2] // sample=*ptrf_use;  // 1 cycle
	vmul.f32 s11,s10,s0 // sum=sample*b0;   // 1
	vfma.f32 s11,s1,s7 // sum+=b1*bprev1;   // 1*4
	vfma.f32 s11,s2,s8 // sum+=b2*bprev2;   
	vfma.f32 s11,s3,s5 // sum+=a1*aprev1;
	vfma.f32 s11,s4,s6 // sum+=a2*aprev2;
	vmov.f32 s6,s5 // aprev2=aprev1;        // 1*4
	vmov.f32 s5,s11 // aprev1=sum;
	vmov.f32 s8,s7 // bprev2=bprev1;
	vmov.f32 s7,s10 // bprev1=sample;
	vstmia.32 r2!, {s5} // *ptrf_use++=aprev1; // 2
	cmp r2,r3 // f=compare(ptrf_use,ptrf_eop);  // 1
	blt gasmbqfloopinner // if(f.less_than) goto gasmbqloopinner; // 1 (2?)

	vstmia.32 r4!, {s5,s6,s7,s8} // *prevs++=aprev1; *prevs++=aprev2; *prevs++=bprev1; *prefs++=bprev2;                    // 5 cycles
	vcmp.f32 s9,#0 // vf=compare(temp1,0);                                                                                  // 1 cycle
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!)
	bne gasmbqfloopmain // if(f.not_equal) goto gasmbqloopmain;                                                             // 1 (2?) cycles
	
	vpop {s0-s11}
	pop {r4}
	bx lr
	
/* void gasmBiquadff(float* dest, float* coefs, float* src); */
 .global gasmBiquadff
.thumb_func
	gasmBiquadff: // makes a copy and only defiles the copy with the effect of filter coefficients.
	
	@ r0: dest
	@ r1: coefs
	@ r2: src
	push {r4}
	vpush {s0-s11}
	push {r5}
	
	add r3,r0,#512 // ptrf_eop=ptrf+128;
// gasmbqloopmain: gasmBiquadff passes to gasmBiquadf where the *dest becomes src/dest, if s9!=0...
	vldmia.32 r1!, {s0,s1,s2,s3,s4} // b0=*coefs++; b1=*coefs++; b2=*coefs++; a1=*coefs++; a2=*coefs++;                    // 6 cycles
	mov r4,r1 // prevs=coefs;                                                                                              // 1 cycle
	vldmia.32 r1!, {s5,s6,s7,s8,s9} // aprev1=*coefs++; aprev2=*coefs++; bprev1=*coefs++; bprev2=*coefs++; temp1=*coefs++; // 6 cycles
	mov r5,r0 // dest_use=dest;                                                                                            // 1 cycle
gasmbqffloopinner: // 15(+/-1) cycles * 128 =~ 1920 cycles
	vldmia.32 r2!, {s10} // sample=*ptrf_use;  // 2 cycles
	vmul.f32 s11,s10,s0 // sum=sample*b0;   // 1
	vfma.f32 s11,s1,s7 // sum+=b1*bprev1;   // 1*4
	vfma.f32 s11,s3,s5 // sum+=a1*aprev1;
	vfma.f32 s11,s4,s6 // sum+=a2*aprev2;
	vfma.f32 s11,s2,s8 // sum+=b2*bprev2;   
	vmov.f32 s6,s5 // aprev2=aprev1;        // 1*4
	vmov.f32 s5,s11 // aprev1=sum;
	vmov.f32 s8,s7 // bprev2=bprev1;
	vmov.f32 s7,s10 // bprev1=sample;
	vstmia.32 r5!, {s5} // *ptrf_use++=aprev1; // 2
	cmp r2,r3 // f=compare(ptrf_use,ptrf_eop);  // 1
	blt gasmbqffloopinner // if(f.less_than) goto gasmbqloopinner; // 1 (2?)

	pop {r5}
	vstmia.32 r4!, {s5,s6,s7,s8} // *prevs++=aprev1; *prevs++=aprev2; *prevs++=bprev1; *prefs++=bprev2;                    // 5 cycles
	vcmp.f32 s9,#0 // vf=compare(temp1,0);                                                                                  // 1 cycle
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!) - extra cycles with what actual value added?
	bne gasmbqfloopmain // if(f.not_equal) goto gasmbqfloopmain;                                                           // 1 (2?) cycles
	
	vpop {s0-s11}
	pop {r4}
	bx lr
	
/* void gasmMix2(float* buff0, float* buff1, float* gain); */
 .global gasmMix2
.thumb_func
	gasmMix2:
	
	@ r0: buff0
	@ r1: buff1
	@ r2: gain

	vpush {s0-s4}

	vldmia.32 r2, {s0,s1} // gain0=gain[0]; gain1=gain[1];
	add r2,r0,#512 // ptr0_eop=ptr0+128;
gasmm2loop: // 1280(+/-128) cycles
	vldr.f32 s2,[r0] // sample0=*ptr0; // 1 cycle
	vldmia.32 r1!, {s3} // sample1=*ptr1++; // 2 cycles
	vmul.f32 s4,s0,s2 // temp=gain0*sample0; // 1 cycle
	vfma.f32 s4,s1,s3 // temp+=gain1*sample1; // 1 cycle
	vstmia.32 r0!, {s4} // *ptr0++=temp; // 2 cycles
	cmp r0,r2 // f=compare(ptr0,ptr0_eop); // 1 cycle
	blt gasmm2loop // if(f.less_than) goto gasmm2loop; // 1 cycle (maybe 2 actually)

	vpop {s0-s4}

	bx lr

/* void void gasmCaterPeaks(float* peakinfo); */
 .global gasmCaterPeaks
.thumb_func
	gasmCaterPeaks:
	
	@ r0: peakinfo
	
	vpush {s0-s3}

	vldmia r0!, {s1,s2,s3} // num_sets=*peakinfo++; dummy=*peakinfo++; divisor=*peakinfo++;
	
	vcvt.u32.f32 s1,s1
	vmov r1,s1
	
gasmcploop:
	vldmia r0!, {s0,s1,s2} // peaklo=*peakinfo++; peakhi=*peakinfo++; peakRatio=*peakinfo++;
	vsub.f32 s0,s1,s0 // peaklo = peakhi-peaklo; // min=0; max=65535; 32767-(-32768)=65535
	vdiv.f32 s0,s0,s3 // peaklo/=65535;
	vcmp.f32 s0,s2 // vf=compare(peaklo,peakRatio); // 1
	vmrs apsr_nzcv, fpscr // transfer vf to f (annoying!)
	blt gasmcpskip
	
	vstr s0, [r1,#-4] // *peakinfoout=peaklo;
gasmcpskip:
	vldmia r0!, {s0,s1} // peaklo=*peakinfoin++; peakhi=*peakinfoin++;
	sub r1,1
	cmp r1,0
	bne gasmcploop // if (f.not_equal) goto gasmcploop
	
	vpop {s0-s3}
	bx lr


#endif