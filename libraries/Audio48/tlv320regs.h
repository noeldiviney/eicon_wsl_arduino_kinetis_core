/* TLV320AIC3254, PCM3070 and certain other compat register map */

/* #define PAGESELREG *,0 // not usable, reg 0 on each page is the page select register for good reason. */
#define SRR 0,1
/* Software Reset Register
D7-D1: Reserved; 0000 000
D0: Self clearing software reset bit [0];
	0: Don't care
	1: Self clearing software reset
*/

#define CSR1 0,4
/* Clock Setting Register 1, Multiplexers
D7: Reserved [0];
D6 Select PLL range [0];
	0: Low PLL clock range
	1: High PLL clock range
D5-D4: Reserved [00];
D3-D2: Select PLL Input Clock [00];
	00: MCLK pin is input to PLL
	01: BCLK pin is input to PLL
	10: GPIO pin ia input to PLL
	11: DIN pin is input to PLL
D1:D0: Select CODEC_CLKIN [00];
	00: MCLK pin is CODEC_CLKIN
	01: BCLK pin is CODEC_CLKIN
	10: GPIO pin is CODEC_CLKIN
	11: PLL Clock is CODEC_CLKIN
*/

#define CSR2 0,5
/* Clock Setting Register 2, PLL P and R Values
D7: PLL Power up [0];
	0: PLL is powered down
	1: PLL is powered up
D6-D4: PLL Divider P value [001];
	000: P=8
	001-111: P=@VALUE binary
D3-D0: PLL divider R value [0001];
	0000: Reserved, no use
	0001-0100: R=@VALUE binary
	0101-1111: Reserved, no use
*/

#define CSR3 0,6
/* Clock Setting Register 3, PLL J Values
D7-D6: Reserved [00];
D5-D0: PLL divider J value [000100];
	000000-000011: Not used!
	000100-111111: J=@VALUE binary
*/

#define CSR4 0,7
/* Clock Setting Register 4, PLL D Values (MSB)
D7-D6: Reserved [00];
D5-D0: MSB D Value [000000]; valid range (when combined with LSB) 0-9999
*/

#define CSR5 0,8
/* Clock Setting Register 5, PLL D Values (LSB)
D7-D0: LSB D Value [00000000]; valid range 0-9999
*/

#define CSR6 0,11
/* Clock Setting Register 6, NDAC Values
D7: NDAC Divider Power Control [0];
	0: NDAC divider powered down
	1: NDAC divider powered up
D6-D0: NDAC Value [0000001];
	0000000: NDAC=128
	0000001-1111111: NDAC=@VALUE binary
*/

#define CSR7 0,12
/* Clock Setting Register 7, MDAC Values
D7: MDAC Divider Power Control [0];
	0: MDAC divider powered down
	1: MDAC divider powered up
D6-D0: MDAC Value [0000001];
	0000000: MDAC=128
	0000001-1111111: MDAC=@VALUE binary
*/

#define DOSR1 0,13
/* DAC OSR Setting Register 1, MSB Value
D7-D2: Reserved [000000];
D1-D0: DAC OSR (DOSR MSB) Setting [00]; see ## in next register
*/

#define DOSR2 0,14
/* DAC OSR Setting Register 2, LSB Value
D7-D0: DAC OSR (DOSR LSB) Setting [10000000];
	00 00000000: DOSR=1024
	00 00000001-11 11111111: DOSR=@VALUE binary
*/

#define MDSP_DICR1 0,15
/* miniDSP_D Instruction Control Register 1 */

#define MDSP_DICR2 0,16
/* miniDSP_D Instruction Control Register 2 */

#define MDSP_DIFSR 0,17
/* miniDSP_D Interpolation Factor Setting Register */

#define CSR8 0,18
/* Clock Setting Register 8, NADC Values
D7: NADC Clock divider power control [0];
	0: Powered Down, ADC_CLK is same as DAC_CLK 
	1: Powered up
D6-D0: NADC Value [0000001];
	0000000: NADC=128
	0000001-1111111: NADC=@VALUE Binary
*/

#define CSR9 0,19
/* Clock Setting Register 9, MADC Values
D7: MADC Clock divider power control [0];
	0: Powered down, ADC_MOD_CLK is same as DAC_MOD_CLK
	1: Powered up
D6-D0: MADC Value [0000001]
	0000000: MADC=128
	0000001-1111111: MADC=@VALUE Binary
*/

#define AOSR 0,20
/* ADC Oversampling (AOSR) Register
D7-D0: ADC Oversampling Value [10000000];
	00000000: AOSR=256
	00000001-11111111: AOSR=@VALUE Binary
*/

#define MDSP_AICR1 0,21
/* miniDSP_A Instruction Control Register 1 */

#define MDSP_AICR2 0,22
/* miniDSP_A Instruction Control Register 2 */

#define MDSP_ADFSR 0,23
/* miniDSP_A Decimation Factor Setting Register */

// 0 24 0x00 0x18 Reserved Register

#define CSR10 0,25
/* Clock Setting Register 10, Multiplexers
D7-D3: Reserved [00000];
D2-D0: CDIV_CLKIN Clock selection [000];
	000: CDIV_CLKIN=MCLK
	001: CDIV_CLKIN=BCLK
	010: CDIV_CLKIN=DIN
	011: CDIV_CLKIN=PLL_CLK
	100: CDIV_CLKIN=DAC_CLK
	101: CDIV_CLKIN=DAC_MOD_CLK
	110: CDIV_CLKIN=ADC_CLK
	111: CDIV_CLKIN=ADC_MOD_CLK
*/

#define CSR11 0,26
/* Clock Setting Register 11, CLKOUT M divider value
D7: CLKOUT M divider power control [0];
	0: CLKOUT M Divider powered down
	1: powered up
D6-D0: CLKOUT M Divider value [0000001];
	0000000: CLKOUT M Divider=128
	0000001-1111111: CLKOUT M Divider=@VALUE Binary
*/

#define AISR1 0,27
/* Audio Interface Setting Register 1
D7-D6: Audio Interface Selection [00];
	00: I2S
	01: DSP
	10: RJF
	11: LJF
D5-D4: Audio Data Word Length [00];
	00: 16 bits
	01: 20 bits
	10: 24 bits
	11: 32 bits
D3: BCLK Direction control [0];
	0: BLCK is input
	1: BCLK is output
D2: WCLK Direction control [0];
	0: WCLK is input
	1: WCLK is output
D1: Reserved [0];
D0: DOUT High Impedance Output Control [0];
	0: DOUT will not be high impedance while Audio Interface is active
	1: DOUT will be high impedance after data has been transferred
*/

#define AISR2 0,28
/* Audio Interface Setting Register 2, Data offset setting
D7-D0: Data offset value [00000000];
	00000000-11111111: Data offset=@VALUE Binary
*/

#define AISR3 0,29
/* Audio Interface Setting Register 3
D7-D6: Reserved [00];
D5: Loopback control (AIF) [0];
	0: No loopback
	1: Audio Data in is routed to Audio Data out (only when WCLK as input)
D4: Loopback control (INTERNAL) [0];
	0: No loopback
	1: Stereo ADC output is routed to Stereo DAC input
D3: Audio Bit Clock Polarity control [0];
	0: Default polarity
	1: Bit clock is inverted w.r.t. default polarity.
D2: Primary BCLK and Primary WCLK Power Control [0];
	0: Primary BCLK AND WCLK buffers are powered down when the codec is powered down
	1: buffers remain powered up when used in clock generation even when codec is powered down
D1-D0: BDIV_CLKIN Multiplexer Control [00];
	00: BDIV_CLKIN=DAC_CLK
	01: BDIV_CLKIN=DAC_MOD_CLK
	10: BDIV_CLKIN=ADC_CLK
	11: BDIV_CLKIN=ADC_MOD_CLK
*/

#define CSR12 0,30
/* Clock Setting Register 12, BCLK N Divider
D7: BCLK N Divider Power Control [0];
	0: Powered Down
	1: Powered Up
D6-D0: BCLK N Divider value [0000001];
	0000000: BCLK N divider=128
	0000001-1000000: BCLK N divider=@VALUE Binary
*/

#define AISR4 0,31
/* Audio Interface Setting Register 4, Secondary Audio Interface
D7: Reserved [0];
D6-D5: Secondary Bit Clock Multiplexer [00];
	00: Secondary Bit Clock=GPIO
	01: Secondary Bit Clock=SCLK
	10: Secondary Bit Clock=MISO
	11: Secondary Bit Clock=DOUT
D4-D3: Secondary Word Clock Multiplexer [00];
	00: Secondary Word Clock=GPIO
	01: Secondary Word Clock=SCLK
	10: Secondary Word Clock=MISO
	11: Secondary Word Clock=DOUT
D2-D1: ADC Word Clock Multiplexer [00];
	00: ADC Work Clock=GPIO
	01: ADC Work Clock=SCLK
	10: ADC Work Clock=MISO
	11: NO USE!
D0: Secondary Data Input Multiplexer [0];
	0: Secondary Data Input=GPIO
	1: Secondary Data Input=SCLK
*/

#define AISR5 0,32
/* Audio Interface Setting Register 5
D7-D4: Reserved [0000];
D3: Primary / Secondary Bit Clock Control [0];
	0: Primary Bit Clock (BCLK) is used for Audio Interface and Clocking
	1: Secondary Bit Clock is used instead
D2: Primary / Secondary Word Clock Control [0];
	0: Primary Word Clock (WCLK) is used for Audio Interface
	1: Secondary Word Clock is used instead
D1: ADC Word Clock Control [0];
	0: ADC Word Clock is same as DAC Word Clock
	1: ADC Word Clock is Secondary ADC Word Clock
D0: Audio Data In Control [0];
	0: DIN is used for Audio Data In
	1: Secondary Data In is used instead
*/

#define AISR6 0,33
/* Audio Interface Setting Register 6
D7: BCLK Output Control [0];
	0: BCLK Output=Generated Primary Bit Clock
	1: BCLK Output=Secondary Bit Clock Input
D6: Secondary Bit Clock Output Control [0];
	0: Secondary Bit Clock=BCLK Input
	1: Secondary Bit Clock=Generated Primary Bit Clock
D5-D4: WCLK Output Control [00];
	00: WCLK Output=Generated DAC_FS
	01: WCLK Output=Generated ADC_FS
	10: WCLK Output=Secondary Word Clck Input
	11: NO USE!
D3-D2: Secondary Word Clock Output Control [00];
	00: Secondary Word Clock Output=WCLK Input
	01: Secondary Word Clock Output=Generated DAC_FS
	10: Secondary Word Clock Output=Generated ADC_FS
	11: NO USE!
D1: Primary Data Out Output Control [0]
	0: DOUT output = Data Output From Serial Interface
	1: DOUT output = Secondary Data Input (loopback)
D0: Secondary Data Out Output Control [0]
	0: Secondary Data Out=DIN input (loopback)
	1: Secondary Data Out=Data output from Serial Interface
*/

#define DIMSR 0,34
/* Digital Interface Misc. Setting Register 
D7-D6: Reserved [00];
D5: I2C General Call Address Configuration [0];
	0: I2C General Call Address will be ignored
	1: I2C General Call Address accepted
D4-D0: Reserved [0000];
*/

// 0 35 0x00 0x23 Reserved Register

#define AFR 0,36
/* ADC Flag Register
D7: Left ADC PGA Status Flag [0];
	0: Gain Applied in Left ADC PGA is not equal to Programmed Gain in Control Register
	1: Gain is equal to programmed.
D6: Left ADC Power Status Flag [0];
	0: Powered Down
	1: Powered up
D5: LEFT AFG Gain Status [0]; (This sticky flag will clear on reading)
	0: Gain in left channel AGC is not saturated
	1: Gain in left ADC is equal to maximum allowed gain in Left AGC
D4: Reserved [0];
D3: Right ADC PGA Status Flag [0];
	0: Gain Applied in Right ADC PGA is not equal to programmed gain in Control register
	1: Gain is equal to programmed.
D2: Right ADC Power Status Flag [0];
	0: Powered down
	1: Powered up
D1: Right AGC Gain Status [0]; (This stick flag will clear on reading)
	0: Gain in Right channel AGC is not saturated
	1: Gain in Right ADC is equal to maximum allowed gain in right AGC
D0: Reserved [0];
*/

#define DFR1 0,37
/* DAC Flag Register 1
D7: Left DAC Power Status Flag [0];
	0: Powered Down
	1: Powered up
D6: Left Line Output Driver (LOL) Power Status Flag [0];
	0: Powered down
	1: Powered up
D5: Left Headphone Driver (HPL) Power Status Flag [0];
	0: Powered down
	1: Powered up
D4: Reserved [0];
D3: Right DAC Power Status Flag [0];
	0: Powered down
	1: powered up
D2: Right Line Output Driver (LOR) Power Status Flag [0];
	0: Powered Down
	1: Powered up
D1: Right Headphone Driver (HPR) Power Status Flag [0];
	0: Powered Down
	1: Powered up
D0: Reserved [0];
*/

#define DFR2 0,38
/* DAC Flag Register 2
D7-D5: Reserved [000];
D4: Left DAC PGA Status Flag [0];
	0: Gain applied in Left DAC PGA is not equal to Gain Programmed in Control Register
	1: Gain as programmed
D3-D1: Reserved [000];
D0: Right DAC PGA Status Flag [0];
	0: Gain applied in Right DAC PGA is not equal to Gain Programmed in Control Register
	1: Gain as programmed
*/

// 0 39-41 0x00 0x27-0x29 Reserved Register

#define SFR1 0,42
/* Sticky Flag Register 1
D7: Left DAC Overflow Status [0]; (self clear on read)
	0: No overflow in Left DAC
	1: Overflow has happened in Left DAC since last read
D6: Right DAC Overflow Status [0]; (self clear on read)
	0: No overflow in Right DAC
	1: Overflow has occurred in Right DAC since last read
D5: miniDSP_D Barrel Shifter Output Overlow [0];
	NO DEFINITION GIVEN
D4: Reserved [0];
D3: Left ADC Overflow Status [0]; (self clear on read)
	0: None
	1: Since last read
D2: Right ADC Overflow Status [0]; (self clear on read)
	0: None
	1: Since last read
D1: miniDSP_A Barrel Shifter Output Overlow [0];
	NO DEFINITION GIVEN
D0: Reserved [0];
*/

#define IFR1 0,43
/* Interrupt Flag Register 1
D7: Left DAC Overflow Status [0];
	0: No overflow in Left DAC
	1: Overflow is in Left DAC
D6: Right DAC Overflow Status [0];
	0: No overflow in Right DAC
	1: Overflow is in Right DAC
D5: miniDSP_D Barrel Shifter Output Overlow [0];
	NO DEFINITION GIVEN
D4: Reserved [0];
D3: Left ADC Overflow Status [0];
	0: None
	1: NOW!
D2: Right ADC Overflow Status [0]; (self clear on read)
	0: None
	1: NOW!
D1: miniDSP_A Barrel Shifter Output Overlow [0];
	NO DEFINITION GIVEN
D0: Reserved [0];
*/

#define SFR2 0,44
/* Sticky Flag Register 2
D7: HPL Over current detect flag [0];
	0: None
	1: Since last read
D6: HPR Over current detect flag [0];
	0: None
	1: Since last read
D5: Headset button press [0];
	0: None
	1: Since last read
D4: Headset insertion/removal detect flag [0];
	0: None
	1: since last read
D3: Left channel DRC, signal threshold flag [0];
	0: None
	1: Since last read
D2: Right channel DRC, signal threshold flag [0];
	0: None
	1: Since last read
D1: miniDSP_D Standard Interrupt Port Output.
	NO DEFINITION GIVEN
D0: miniDSP_D Auxillary Interrupt Port Output
	NO DEFINITION GIVEN
*/

#define SFR3 0,45
/* Sticky Flag Register 3
D7 R 0 Reserved. Write only default values
D6 R 0 Left AGC Noise Threshold Flag
	0: Signal Power is greater than Noise Threshold
	1: Signal Power was lower than Noise Threshold (will be cleared when the register is read)
D5 R 0 Right AGC Noise Threshold Flag
	0: Signal Power is greater than Noise Threshold
	1: Signal Power was lower than Noise Threshold (will be cleared when the register is read)
D4 R 0 miniDSP_A Standard Interrupt Port Output. This is a sticky bit
D3 R 0 miniDSP_A Auxilliary Interrupt Port Output. This is a sticky bit
D2 R 0 Left ADC DC Measurement Data Available Flag
	0: Data not available
	1: Data available (will be cleared when the register is read)
D1 R 0 Right ADC DC Measurement Data Available Flag
	0: Data not available
	1: Data available (will be cleared when the register is read)
D0 R 0 Reserved. Write only default values
*/

#define IFR2 0,46
/* Interrupt Flag Register 2
D7 R 0 HPL Over Current Detect Flag
	0: Over Current not detected on HPL
	1: Over Current detected on HPL
D6 R 0 HPR Over Current Detect Flag
	0: Over Current not detected on HPR
	1: Over Current detected on HPR
D5 R 0 Headset Button Press
	0: Button Press not detected
	1: Button Press detected
D4 R 0 Headset Insertion/Removal Detect Flag
	0: Headset removal detected
	1: Headset insertion detected
D3 R 0 Left Channel DRC, Signal Threshold Flag
	0: Signal Power is below Signal Threshold
	1: Signal Power exceeded Signal Threshold
D2 R 0 Right Channel DRC, Signal Threshold Flag
	0: Signal Power is below Signal Threshold
	1: Signal Power exceeded Signal Threshold
D1 R 0 miniDSP_D Standard Interrupt Port Output.
	This bit shows the instantaneous value of miniDSP interrupt port at the time of reading the register
D0 R 0 miniDSP_D Auxilliary Interrupt Port Output.
	This bit shows the instantaneous value of miniDSP interrupt port at the time of reading the register
*/

#define IFR3 0,47
/* Interrupt Flag Register 3
D7 R 0 Reserved. Write only default values
D6 R 0 Left AGC Noise Threshold Flag
	0: Signal Power is greater than Noise Threshold
	1: Signal Power was lower than Noise Threshold
D5 R 0 Right AGC Noise Threshold Flag
	0: Signal Power is greater than Noise Threshold
	1: Signal Power was lower than Noise Threshold
D4 R 0 miniDSP_A Standard Interrupt Port Output.
	This bit shows the instantaneous value of the interrupt port at the time of reading the register
D3 R 0 miniDSP_A Auxilliary Interrupt Port Output.
	This bit shows the instantaneous value of the interrupt port at the time of reading the register
D2 R 0 Left ADC DC Measurement Data Available Flag
	0: Data not available
	1: Data available
D1 R 0 Right ADC DC Measurement Data Available Flag
	0: Data not available
	1: Data available
D0 R 0 Reserved. Write only default values
*/

#define INT1ICR 0,48
/* INT1 Interrupt Control Register
D7 R/W 0 INT1 Interrupt for Headset Insertion Event
	0: Headset Insertion event will not generate an INT1 interrupt
	1: Headset Insertion even will generate an INT1 interrupt
D6 R/W 0 INT1 Interrupt for Button Press Event
	0: Button Press event will not generate an INT1 interrupt
	1: Button Press event will generate an INT1 interrupt
D5 R/W 0 INT1 Interrupt for DAC DRC Signal Threshold
	0: DAC DRC Signal Power exceeding Signal Threshold will not generate an INT1 interrupt
	1: DAC DRC Signal Power exceeding Signal Threshold for either of Left or Right Channel will
	generate an INT1 interrupt.
	Read Page-0, Register-44 to distinguish between Left or Right Channel
D4 R/W 0 INT1 Interrupt for AGC Noise Interrupt
	0: Noise level detected by AGC will not generate an INT1 interrupt
	1: Noise level detected by either off Left or Right Channel AGC will generate an INT1 interrupt.
	Read Page-0, Register-45 to distinguish between Left or Right Channel
D3 R/W 0 INT1 Interrupt for Over Current Condition
	0: Headphone Over Current condition will not generate an INT1 interrupt.
	1: Headphone Over Current condition on either off Left or Right Channels will generate an INT1
	interrupt.
	Read Page-0, Register-44 to distinguish between HPL and HPR
D2 R/W 0 INT1 Interrupt for overflow event
	0: miniDSP_A or miniDSP_D generated interrupt does not result in an INT1 interrupt
	1: miniDSP_A or miniDSP_D generated interrupt will result in an INT1 interrupt.
	Read Page-0, Register-42 to distinguish between miniDSP_A or miniDSP_D interrupt
D1 R/W 0 INT1 Interrupt for DC Measurement
	0: DC Measurement data available will not generate INT1 interrupt
	1: DC Measurement data available will generate INT1 interrupt
D0 R/W 0 INT1 pulse control
	0: INT1 is active high interrupt of 1 pulse of approx. 2ms duration
	1: INT1 is active high interrupt of multiple pulses, each of duration 2ms. To stop the pulse train,
	read Page-0, Reg-42d, 44d or 45d
*/

#define INT2ICR 0,49
/* INT2 Interrupt Control Register

D7 R/W 0 INT2 Interrupt for Headset Insertion Event
	0: Headset Insertion event will not generate an INT2 interrupt
	1: Headset Insertion even will generate an INT2 interrupt
D6 R/W 0 INT2 Interrupt for Button Press Event
	0: Button Press event will not generate an INT2 interrupt
	1: Button Press event will generate an INT2 interrupt
D5 R/W 0 INT2 Interrupt for DAC DRC Signal Threshold
	0: DAC DRC Signal Power exceeding Signal Threshold will not generate an INT2 interrupt
	1: DAC DRC Signal Power exceeding Signal Threshold for either of Left or Right Channel will
	generate an INT2 interrupt.
	Read Page-0, Register-44 to distinguish between Left or Right Channel
D4 R/W 0 INT2 Interrupt for AGC Noise Interrupt
	0: Noise level detected by AGC will not generate an INT2 interrupt
	1: Noise level detected by either off Left or Right Channel AGC will generate an INT2 interrupt.
	Read Page-0, Register-45 to distinguish between Left or Right Channel
D3 R/W 0 INT2 Interrupt for Over Current Condition
	0: Headphone Over Current condition will not generate an INT2 interrupt.
	1: Headphone Over Current condition on either off Left or Right Channels will generate an INT2
	interrupt.
	Read Page-0, Register-44 to distinguish between HPL and HPR
D2 R/W 0 INT2 Interrupt for overflow event
	0: miniDSP_A or miniDSP_D generated interrupt will not result in an INT2 interrupt
	1: miniDSP_A or miniDSP_D generated interrupt will result in an INT2 interrupt.
	Read Page-0, Register-42 to distinguish between miniDSP_A or miniDSP_D interrupt
D1 R/W 0 INT2 Interrupt for DC Measurement
	0: DC Measurement data available will not generate INT2 interrupt
	1: DC Measurement data available will generate INT2 interrupt
D0 R/W 0 INT2 pulse control
	0: INT2 is active high interrupt of 1 pulse of approx. 2ms duration
	1: INT2 is active high interrupt of multiple pulses, each of duration 2ms. To stop the pulse train,
	read Page-0, Reg-42d, 44d and 45d
*/

// 0 50-51 0x00 0x32-0x33 Reserved Register

#define MFP5CR 0,52
/* GPIO/MFP5 Control Register
D7-D6 R 00 Reserved. Write only default values
D5-D2 R/W 0000 GPIO Control
	0000: GPIO input/output disabled.
	0001: GPIO input is used for secondary audio interface, digital microphone input or clock input.
		Configure other registers to choose the functionality of GPIO input
	0010: GPIO is general purpose input
	0011: GPIO is general purpose output
	0100: GPIO output is CLKOUT
	0101: GPIO output is INT1
	0110: GPIO output is INT2
	0111: GPIO output is ADC_WCLK for Audio Interface
	1000: GPIO output is secondary bit-clock for Audio Interface
	1001: GPIO output is secondary word-clock for Audio Interface
	1010: GPIO output is clock for digital microphone
	1011-1111: Reserved. Do not use.
D1 R X GPIO Input Pin state, used along with GPIO as general purpose input
D0 R/W 0 GPIO as general purpose output control
	0: GPIO pin is driven to '0' in general purpose output mode
	1: GPIO pin is driven to '1' in general purpose output mode
*/

#define MFP2FCR 0,53
/* DOUT/MFP2 Function Control Register
D7-D5 R 000 Reserved. Write only default values
D4 R/W 1 DOUT Bus Keeper Control
	0: DOUT Bus Keeper Enabled
	1: DOUT Bus Keeper Disabled
D3-D1 R/W 001 DOUT MUX Control
	000: DOUT disabled
	001: DOUT is Primary DOUT
	010: DOUT is General Purpose Output
	011: DOUT is CLKOUT
	100: DOUT is INT1
	101: DOUT is INT2
	110: DOUT is Secondary BCLK
	111: DOUT is Secondary WCLK
D0 R/W 0 DOUT as General Purpose Output
	0: DOUT General Purpose Output is '0'
	1: DOUT General Purpose Output is '1'
*/

#define MFP1FCR 0,54
/* DIN/MFP1 Function Control Register
D7-D3 R 0 0000 Reserved. Write only reserved values
D2-D1 R/W 01 DIN function control
	00: DIN pin is disabled
	01: DIN is enabled for Primary Data Input or Digital Microphone Input or General Purpose Clock input
	10: DIN is used as General Purpose Input
	11: Reserved. Do not use
D0 R X Value of DIN input pin. To be used when for General Purpose Input
*/

#define MFP4FCR 0,55
/* MISO/MFP4 Function Control Register
D7-D5 R 000 Reserved. Write only default values
D4-D1 R/W 0001 MISO function control
	0000: MISO buffer disabled
	0001: MISO is used for data output in SPI interface, is disabled for I2C interface
	0010: MISO is General Purpose Output
	0011: MISO is CLKOUT output
	0100: MISO is INT1 output
	0101: MISO is INT2 output
	0110: MISO is ADC Word Clock output
	0111: MISO is clock output for Digital Microphone
	1000: MISO is Secondary Data Output for Audio Interface
	1001: MISO is Secondary Bit Clock for Audio Interface
	1010: MISO is Secondary Word Clock for Audio Interface
	1011-1111: Reserved. Do not use
D0 R/W 0 Value to be driven on MISO pin when used as General Purpose Output
*/

#define MFP3FCR 0,56
/* SCLK/MFP3 Function Control Register
D7-D3 R 0 0000 Reserved. Write only default values
D2-D1 R/W 01 SCLK function control
	00: SCLK pin is disabled
	01: SCLK pin is enabled for SPI clock in SPI Interface mode or when in I2C Interface enabled for
		Secondary Data Input or Secondary Bit Clock Input or Secondary Word Clock or Secondary ADC
		Word Clock or Digital Microphone Input
	10: SCLK is enabled as General Purpose Input
	11: Reserved. Do not use
D0 R X Value of SCLK input pin when used as General Purpose Input
*/

// 0 57-59 0x00 0x39-0x3B Reserved Registers

#define DSPBCR 0,60
/* DAC Signal Processing Block Control Register
D7 R/W 0
	0: miniDSP_A and miniDSP_D are independently powered up
	1: miniDSP_A and miniDSP_D are powered up together. Useful when there is data transfer
		between miniDSP_A and miniDSP_D
D6 R/W 0 miniDSP_D Power Configuration
	0: miniDSP_D is powered down with DAC Channel Power Down
	1: miniDSP_D is powered up if ADC Channel is powered up
D5 R 0 Reserved. Write only default value
D4-D0 R/W 0 0001
	0 0000: The miniDSP_D will be used for signal processing
	0 0001: DAC Signal Processing Block PRB_P1
	0 0010: DAC Signal Processing Block PRB_P2
	0 0011: DAC Signal Processing Block PRB_P3
	0 0100: DAC Signal Processing Block PRB_P4
	�
	1 1000: DAC Signal Processing Block PRB_P24
	1 1001: DAC Signal Processing Block PRB_P25
	1 1010-1 1111: Reserved. Do not use
*/

#define ASPBCR 0,61
/* ADC Signal Processing Block Control Register
D7-D5 R 000 Reserved. Write only default values
D4-D0 R/W 0 0001
	0 0000: The miniDSP_A will be used for signal processing
	0 0001: ADC Singal Processing Block PRB_R1
	0 0010: ADC Signal Processing Block PRB_R2
	0 0011: ADC Signal Processing Block PRB_R3
	0 0100: ADC Signal Processing Block PRB_R4
	�
	1 0001: ADC Signal Processing Block PRB_R17
	1 0010: ADC Signal Processing Block PRB_R18
	1 0010-1 1111: Reserved. Do not use
*/

#define MDSP_ADCR 0,62
/* miniDSP_A and miniDSP_D Configuration Register
D7 R 0 Reserved. Write only default values
D6 R/W 0 miniDSP_A Auxilliary Control Bit-A. Used for conditional instruction like JMP.
D5 R/W 0 miniDSP_A Auxilliary Control Bit-B. Used for conditional instruction like JMP.
D4 R/W 0
	0: Reset miniDSP_A instruction counter at the start of new frame.
	1: Do not reset miniDSP_A instruction counter at the start of new frame. If miniDSP_A is used for
		Signal Processing
D3 R 0 Reserved. Write only default values
D2 R/W 0 miniDSP_D Auxilliary Control Bit-A. Used for conditional instruction like JMP.
D1 R/W 0 miniDSP_D Auxilliary Control Bit-B. Used for conditional instruction like JMP.
D0 R/W 0
	0: Reset miniDSP_D instruction counter at the start of new frame.
	1: Do not reset miniDSP_D instruction counter at the start of new frame. If miniDSP_D is used for
		Signal Processing
*/

#define DCSR1 0,63
/* DAC Channel Setup Register 1
D7 R/W 0 Left DAC Channel Power Control
	0: Left DAC Channel Powered Down
	1: Left DAC Channel Powered Up
D6 R/W 0 Right DAC Channel Power Control
	0: Right DAC Channel Powered Down
	1: Right DAC Channel Powered Up
D5-D4 R/W 01 Left DAC Data path Control
	00: Left DAC data is disabled
	01: Left DAC data Left Channel Audio Interface Data
	10: Left DAC data is Right Channel Audio Interface Data
	11: Left DAC data is Mono Mix of Left and Right Channel Audio Interface Data
D3-D2 R/W 01 Right DAC Data path Control
	00: Right DAC data is disabled
	01: Right DAC data Right Channel Audio Interface Data
	10: Right DAC data is Left Channel Audio Interface Data
	11: Right DAC data is Mono Mix of Left and Right Channel Audio Interface Data
D1-D0 R/W 00 DAC Channel Volume Control's Soft-Step control
	00: Soft-Stepping is 1 step per 1 DAC Word Clock
	01: Soft-Stepping is 1 step per 2 DAC Word Clocks
	10: Soft-Stepping is disabled
	11: Reserved. Do not use
*/
#define DCSR2 0,64
/* DAC Channel Setup Register 2
D7 R/W 0 Right Modulator Output Control
	0: When Right DAC Channel is powered down, the data is zero.
	1: When Right DAC Channel is powered down, the data is inverted version of Left DAC Modulator
	Output. Can be used when differential mono output is used
D6-D4 R/W 000 DAC Auto Mute Control
	000: Auto Mute disabled
	001: DAC is auto muted if input data is DC for more than 100 consecutive inputs
	010: DAC is auto muted if input data is DC for more than 200 consecutive inputs
	011: DAC is auto muted if input data is DC for more than 400 consecutive inputs
	100: DAC is auto muted if input data is DC for more than 800 consecutive inputs
	101: DAC is auto muted if input data is DC for more than 1600 consecutive inputs
	110: DAC is auto muted if input data is DC for more than 3200 consecutive inputs
	111: DAC is auto muted if input data is DC for more than 6400 consecutive inputs
D3 R/W 1 Left DAC Channel Mute Control
	0: Left DAC Channel not muted
	1: Left DAC Channel muted
D2 R/W 1 Right DAC Channel Mute Control
	0: Right DAC Channel not muted
	1: Right DAC Channel muted
D1-D0 R/W 00 DAC Master Volume Control
	00: Left and Right Channel have independent volume control
	01: Left Channel Volume is controlled by Right Channel Volume Control setting
	10: Right Channel Volume is controlled by Left Channel Volume Control setting
	11: Reserved. Do not use
*/

#define LDCDVCR 0,65
/* Left DAC Channel Digital Volume Control Register
D7-D0 R/W 0000 0000 Left DAC Channel Digital Volume Control Setting
	0111 1111-0011 0001: Reserved. Do not use
	0011 0000: Digital Volume Control = +24dB
	0010 1111: Digital Volume Control = +23.5dB
	�
	0000 0001: Digital Volume Control = +0.5dB
	0000 0000: Digital Volume Control = 0.0dB
	1111 1111: Digital Volume Control = -0.5dB
	...
	1000 0010: Digital Volume Control = -63dB
	1000 0001: Digital Volume Control = -63.5dB
	1000 0000: Reserved. Do not use
*/

#define RDCDVCR 0,66
/* Right DAC Channel Digital Volume Control Register
	As LDCDVCR.
*/


#define HDCR 0,67
/* Headset Detection Configuration Register
D7 R/W 0
	0: Headset Detection Disabled
	1: Headset Detection Enabled
D6-D5 R 00 Headset Type Flag
	00: Headset not detected
	01: Stereo Headset detected
	10: Reserved
	11: Stereo + Cellular Headset detected
D4-D2 R/W 000 Headset Detection Debounce Programmability
	000: Debounce Time = 16ms
	001: Debounce Time = 32ms
	010: Debounce Time = 64ms
	011: Debounce Time = 128ms
	100: Debounce Time = 256ms
	101: Debounce Time = 512ms
	110-111: Reserved. Do not use
	Note: All times are typical values
D1-D0 R/W 00 Headset Button Press Debounce Programmability
	00: Debounce disabled
	01: Debounce Time = 8ms
	10: Debounce Time = 16ms
	11: Debounce Time = 32ms
	Note: All times are typical values
*/

#define DRCCR1 0,68
/* DRC Control Register 1
D7 R 0 Reserved. Write only default value
D6 R/W 1 DRC Enable Control
	0: Left Channel DRC disabled
	1: Left Channel DRC enabled
	Note: DRC only active if a PRB_Px has been selected that supports DRC
D5 R/W 1 DRC Enable Control
	0: Right Channel DRC disabled
	1: Right Channel DRC enabled
	Note: DRC only active if a PRB_Px has been selected that supports DRC
D4-D2 R/W 011 DRC Threshold control
	000: DRC Threshold = -3dBFS
	001: DRC Threshold = -6dBFS
	010: DRC Threshold = -9dBFS
	011: DRC Threshold = -12dBFS
	100: DRC Threshold = -15dBFS
	101: DRC Threshold = -18dBFS
	110: DRC Threshold = -21dBFS
	111: DRC Threshold = -24dBFS
D1-D0 R/W 11 DRC Hysteresis Control
	00: DRC Hysteresis = 0dB
	01: DRC Hysteresis = 1dB
	10: DRC Hysteresis = 2dB
	11: DRC Hysteresis = 3dB
*/

#define DRCCR2 0,69
/* DRC Control Register 2
D7 R 0 Reserved. Write only default value.
D6-D3 R/W 0111 DRC Hold Programmability
	0000: DRC Hold Disabled
	0001: DRC Hold Time = 32 DAC Word Clocks
	0010: DRC Hold Time = 64 DAC Word Clocks
	0011: DRC Hold Time = 128 DAC Word Clocks
	0100: DRC Hold Time = 256 DAC Word Clocks
	0101: DRC Hold Time = 512 DAC Word Clocks
	...
	1110: DRC Hold Time = 4 * 32768 DAC Word Clocks
	1111: DRC Hold Time = 5 * 32768 DAC Word Clocks
D2-D0 R/W 000 Reserved. Write only default values
*/

#define DRCCR3 0,70
/* DRC Control Register 3
D7-D4 R/W 0000 DRC Attack Rate control
	0000: DRC Attack Rate = 4.0dB per DAC Word Clock
	0001: DRC Attack Rate = 2.0dB per DAC Word Clock
	0010: DRC Attack Rae = 1.0dB per DAC Word Clock
	�
	1110: DRC Attack Rate = 2.4414e-4dB per DAC Word Clock
	1111: DRC Attack Rate = 1.2207e-4dB per DAC Word Clock
D3-D0 R/W 0000 DRC Decay Rate control
	0000: DRC Decay Rate = 1.5625e-2dB per DAC Word Clock
	0001: DRC Decay Rate = 7.8125e-3dB per DAC Word Clock
	0010: DRC Decay Rae = 3.9062e-3dB per DAC Word Clock
	�
	1110: DRC Decay Rate = 9.5367e-7dB per DAC Word Clock
	1111: DRC Decay Rate = 4.7683e-7dB per DAC Word Clock
*/


#define BGR1 0,71
/* Beep Generator Register 1
D7 R/W 0
	0: Beep Generator Disabled
	1: Beep Generator Enabled. This bit will self clear after the beep has been generated.
D6 R 0 Reserved. Write only default value
D5-D0 R/W 00 0000 Left Channel Beep Volume Control
	00 0000: Left Channel Beep Volume = 0dB
	00 0001: Left Channel Beep Volume = -1dB
	�
	11 1110: Left Channel Beep Volume = -62dB
	11 1111: Left Channel Beep Volume = -63dB
*/

#define BGR2 0,72
/* Beep Generator Register 2
D7-D6 R/W 00 Beep Generator Master Volume Control Setting
	00: Left and Right Channels have independent Volume Settings
	01: Left Channel Beep Volume is the same as programmed for Right Channel
	10: Right Channel Beep Volume is the same as programmed for Left Channel
	11: Reserved. Do not use
D5-D0 R 00 0000 Right Channel Beep Volume Control
	00 0000: Right Channel Beep Volume = 0dB
	00 0001: Right Channel Beep Volume = -1dB
	�
	11 1110: Right Channel Beep Volume = -62dB
	11 1111: Right Channel Beep Volume = -63dB
*/

#define BGR3 0,73
/* Beep Generator Register 3
D7-D0 R/W 0000 0000 Programmed value is Beep Sample Length(23:16)
*/

#define BGR4 0,74
/* Beep Generator Register 4
D7-D0 R/W 0000 0000 Programmed value is Beep Sample Length(15:8)
*/

#define BGR5 0,75
/* Beep Generator Register 5
D7-D0 R/W 1110 1110 Programmed value is Beep Sample Length(7:0)
*/

#define BGR6 0,76
/* Beep Generator Register 6
D7-D0 R/W 0001 0000 Programmed Value is Beep Sin(x)(15:8), where
	Sin(x) = sin(2 * p * Fin / Fs), where Fin is desired beep frequency and Fs is DAC sample rate
*/

#define BGR7 0,77
/* Beep Generator Register 7
D7-D0 R/W 1101 1000 Programmed Value is Beep Sin(x)(7:0), where
	Sin(x) = sin(2 * p * Fin / Fs), where Fin is desired beep frequency and Fs is DAC sample rate
*/

#define BGR8 0,78
/* Beep Generator Register 8
D7-D0 R/W 0111 1110 Programmed Value is Beep Cos(x)(15:8), where
	Cos(x) = cos(2 * p * Fin / Fs), where Fin is desired beep frequency and Fs is DAC sample rate
*/

#define BGR9 0,79
/* Beep Generator Register 9
D7-D0 R/W 1110 0011 Programmed Value is Beep Cos(x)(7:0), where
	Cos(x) = cos(2 * p * Fin / Fs), where Fin is desired beep frequency and Fs is DAC sample rate
*/


// 0 80 0x00 0x50 Reserved Register

#define ACSR 0,81
/* ADC Channel Setup Register
D7 R/W 0 Left Channel ADC Power Control
	0: Left Channel ADC is powered down
	1: Left Channel ADC is powered up
D6 R/W 0 Right Channel ADC Power Control
	0: Right Channel ADC is powered down
	1: Right Channel ADC is powered up
D5-D4 R/W 00 Digital Microphone Input Configuration
	00: GPIO serves as Digital Microphone Input
	01: SCLK serves as Digital Microphone Input
	10: DIN serves as Digital Microphone Input
	11: Reserved. Do not use
D3 R/W 0 Left Channel Digital Microphone Power Control
	0: Left Channel ADC not configured for Digital Microphone
	1: Left Channel ADC configured for Digital Microphone
D2 R/W 0 Right Channel Digital Microphone Power Control
	0: Right Channel ADC not configured for Digital Microphone
	1: Right Channel ADC configured for Digital Microphone
D1-D0 R/W 00 ADC Volume Control Soft-Stepping Control
	00: ADC Volume Control changes by 1 gain step per ADC Word Clock
	01: ADC Volume Control changes by 1 gain step per two ADC Word Clocks
	10: ADC Volume Control Soft-Stepping disabled
	11: Reserved. Do not use
*/

#define AFGAR 0,82
/* ADC Fine Gain Adjust Register
D7 R/W 1 Left ADC Channel Mute Control
	0: Left ADC Channel Un-muted
	1: Left ADC Channel Muted
D6-D4 R/W 000 Left ADC Channel Fine Gain Adjust
	000: Left ADC Channel Fine Gain = 0dB
	111: Left ADC Channel Fine Gain = -0.1dB
	110: Left ADC Channel Fine Gain = -0.2dB
	101: Left ADC Channel Fine Gain = -0.3dB
	100: Left ADC Channel Fine Gain = -0.4dB
	001-011: Reserved. Do not use
D3 R/W 1 Right ADC Channel Mute Control
	0: Right ADC Channel Un-muted
	1: Right ADC Channel Muted
D2-D0 R/W 000 Right ADC Channel Fine Gain Adjust
	000: Right ADC Channel Fine Gain = 0dB
	111: Right ADC Channel Fine Gain = -0.1dB
	110: Right ADC Channel Fine Gain = -0.2dB
	101: Right ADC Channel Fine Gain = -0.3dB
	100: Right ADC Channel Fine Gain = -0.4dB
	001-011: Reserved. Do not use
*/

#define LACVCR 0,83
/* Left ADC Channel Volume Control Register
D7 R 0 Reserved. Write only default values
D6-D0 R/W 000 0000 Left ADC Channel Volume Control
	100 0000-110 0111: Reserved. Do not use
	110 1000: Left ADC Channel Volume = -12dB
	110 1001: Left ADC Channel Volume = -11.5dB
	110 1010: Left ADC Channel Volume = -11.0dB
	�
	111 1111: Left ADC Channel Volume = -0.5dB
	000 0000: Left ADC Channel Volume = 0.0dB
	000 0001: Left ADC Channel Volume = 0.5dB
	...
	010 0110: Left ADC Channel Volume = 19.0dB
	010 0111: Left ADC Channel Volume = 19.5dB
	010 1000: Left ADC Channel Volume = 20.0dB
	010 1001-111 1111: Reserved. Do not use
*/

#define RACVCR 0,84
/* Right ADC Channel Volume Control Register
D7 R 0 Reserved. Write only default values
D6-D0 R/W 000 0000 Right ADC Channel Volume Control
	100 0000-110 0111: Reserved. Do not use
	110 1000: Right ADC Channel Volume = -12dB
	110 1001: Right ADC Channel Volume = -11.5dB
	110 1010: Right ADC Channel Volume = -11.0dB
	�
	111 1111: Right ADC Channel Volume = -0.5dB
	000 0000: Right ADC Channel Volume = 0.0dB
	000 0001: Right ADC Channel Volume = 0.5dB
	...
	010 0110: Right ADC Channel Volume = 19.0dB
	010 0111: Right ADC Channel Volume = 19.5dB
	010 1000: Right ADC Channel Volume = 20.0dB
	010 1001-111 1111: Reserved. Do not use
*/

#define APAR 0,85
/* ADC Phase Adjust Register
D7-D0 R/W 0000 0000 ADC Phase Compensation Control
	1000 0000-1111 1111: Left ADC Channel Data is delayed with respect to Right ADC Channel
		Data. For details of delayed amount please refer to the description of Phase Compensation in the
		Overview section.
	0000 0000: Left and Right ADC Channel data are not delayed with respect to each other
	0000 0001-0111 1111: Right ADC Channel Data is delayed with respect to Left ADC Channel
		Data. For details of delayed amount please refer to the description of Phase Compensation in the
		Overview section.
*/


#define LCAGCCR1 0,86
/* Left Channel AGC Control Register 1
D7 R/W 0
	0: Left Channel AGC Disabled
	1: Left Channel AGC Enabled
D6-D4 R/W 000 Left Channel AGC Target Level Setting
	000: Left Channel AGC Target Level = -5.5dBFS
	001: Left Channel AGC Target Level = -8.0dBFS
	010: Left Channel AGC Target Level = -10.0dBFS
	011: Left Channel AGC Target Level = -12.0dBFS
	100: Left Channel AGC Target Level = -14.0dBFS
	101: Left Channel AGC Target Level = -17.0dBFS
	110: Left Channel AGC Target Level = -20.0dBFS
	111: Left Channel AGC Target Level = -24.0dBFS
D3-D2 R 00 Reserved. Write only default values
D1-D0 R/W 00 Left Channel AGC Gain Hysteresis Control
	00: Left Channel AGC Gain Hysteresis is disabled
	01: Left Channel AGC Gain Hysteresis is �0.5dB
	10: Left Channel AGC Gain Hysteresis is �1.0dB
	11: Left Channel AGC Gain Hysteresis is �1.5dB
*/

#define LCAGCCR2 0,87
/* Left Channel AGC Control Register 2
D7-D6 R/W 00 Left Channel AGC Hysteresis Setting
	00: Left Channel AGC Hysteresis is 1.0dB
	01: Left Channel AGC Hysteresis is 2.0dB
	10: Left Channel AGC Hysteresis is 4.0dB
	11: Left Channel AGC Hysteresis is disabled
D5-D1 R/W 0 0000 Left Channel AGC Noise Threshold
	0 0000: Left Channel AGC Noise Gate disabled
	0 0001: Left Channel AGC Noise Threshold is -30dB
	0 0010: Left Channel AGC Noise Threshold is -32dB
	0 0011: Left Channel AGC Noise Threshold is -34dB
	�
	1 1101: Left Channel AGC Noise Threshold is -86dB
	1 1110: Left Channel AGC Noise Threshold is -88dB
	1 1111: Left Channel AGC Noise Threshold is -90dB
D0 R 0 Reserved. Write only default value
*/

#define LCAGCCR3 0,88
/* Left Channel AGC Control Register 3
D7 R 0 Reserved. Write only default value
D6-D0 R/W 111 1111 Left Channel AGC Maximum Gain Setting
	000 0000: Left Channel AGC Maximum Gain = 0.0dB
	000 0001: Left Channel AGC Maximum Gain = 0.5dB
	000 0010: Left Channel AGC Maximum Gain = 1.0dB
	�
	111 0011: Left Channel AGC Maximum Gain = 57.5dB
	111 0100: Left Channel AGC Maximum Gain = 58.0dB
	111 0101-111 1111: not recommended for usage, Left Channel AGC Maximum Gain = 58.0dB
*/

#define LCAGCCR4 0,89
/* Left Channel AGC Control Register 4
D7-D3 R/W 0 0000 Left Channel AGC Attack Time Setting
	0 0000: Left Channel AGC Attack Time = 1 * 32 ADC Word Clocks
	0 0001: Left Channel AGC Attack Time = 3 * 32 ADC Word Clocks
	0 0010: Left Channel AGC Attack Time = 5 * 32 ADC Word Clocks
	�
	1 1101: Left Channel AGC Attack Time = 59 * 32 ADC Word Clocks
	1 1110: Left Channel AGC Attack Time = 61 * 32 ADC Word Clocks
	1 1111: Left Channel AGC Attack Time = 63 * 32 ADC Word Clocks
D2-D0 R/W 000 Left Channel AGC Attack Time Scale Factor Setting
	000: Scale Factor = 1
	001: Scale Factor = 2
	010: Scale Factor = 4
	�
	101: Scale Factor = 32
	110: Scale Factor = 64
	111: Scale Factor = 128
*/

#define LCAGCCR5 0,90
/* Left Channel AGC Control Register 5
D7-D3 R/W 0 0000 Left Channel AGC Decay Time Setting
	0 0000: Left Channel AGC Decay Time = 1 * 512 ADC Word Clocks
	0 0001: Left Channel AGC Decay Time = 3 * 512 ADC Word Clocks
	0 0010: Left Channel AGC Decay Time = 5 * 512 ADC Word Clocks
	�
	1 1101: Left Channel AGC Decay Time = 59 * 512 ADC Word Clocks
	1 1110: Left Channel AGC Decay Time = 61 * 512 ADC Word Clocks
	1 1111: Left Channel AGC Decay Time = 63 * 512 ADC Word Clocks
D2-D0 R/W 000 Left Channel AGC Decay Time Scale Factor Setting
	000: Scale Factor = 1
	001: Scale Factor = 2
	010: Scale Factor = 4
	�
	101: Scale Factor = 32
	110: Scale Factor = 64
	111: Scale Factor = 128
*/

#define LCAGCCR6 0,91
/* Left Channel AGC Control Register 6
D7-D5 R 000 Reserved. Write only default values
D4-D0 R/W 0 0000 Left Channel AGC Noise Debounce Time Setting
	0 0001: Left Channel AGC Noise Debounce Time = 0
	0 0010: Left Channel AGC Noise Debounce Time = 4 ADC Word Clocks
	0 0011: Left Channel AGC Noise Debounce Time = 8 ADC Word Clocks
	�
	0 1010: Left Channel AGC Noise Debounce Time = 2048 ADC Word Clocks
	0 1011: Left Channel AGC Noise Debounce Time = 4096 ADC Word Clocks
	0 1100: Left Channel AGC Noise Debounce Time = 2 * 4096 ADC Word Clocks
	0 1101: Left Channel AGC Noise Debounce Time = 3 * 4096 ADC Word Clocks
	...
	1 1101: Left Channel AGC Noise Debounce Time = 19 * 4096 ADC Word Clocks
	1 1110: Left Channel AGC Noise Debounce Time = 20 * 4096 ADC Word Clocks
	1 1111: Left Channel AGC Noise Debounce Time = 21 * 4096 ADC Word Clocks
*/

#define LCAGCCR7 0,92
/* Left Channel AGC Control Register 7
D7-D4 R 0000 Reserved. Write only default values
D3-D0 R/W 0000 Left Channel AGC Signal Debounce Time Setting
	0001: Left Channel AGC Signal Debounce Time = 0
	0010: Left Channel AGC Signal Debounce Time = 4 ADC Word Clocks
	0011: Left Channel AGC Signal Debounce Time = 8 ADC Word Clocks
	�
	1001: Left Channel AGC Signal Debounce Time = 1024 ADC Word Clocks
	1010: Left Channel AGC Signal Debounce Time = 2048 ADC Word Clocks
	1011: Left Channel AGC Signal Debounce Time = 2 * 2048 ADC Word Clocks
	1100: Left Channel AGC Signal Debounce Time = 3 * 2048 ADC Word Clocks
	1101: Left Channel AGC Signal Debounce Time = 4 * 2048 ADC Word Clocks
	1110: Left Channel AGC Signal Debounce Time = 5 * 2048 ADC Word Clocks
	1111: Left Channel AGC Signal Debounce Time = 6 * 2048 ADC Word Clocks
*/

#define LCAGCCR8 0,93
/* Left Channel AGC Control Register 8
D7-D0 R 0000 0000 Left Channel AGC Gain Flag
	1110 1000: Left Channel AGC Gain = -12.0dB
	1110 1001: Left Channel AGC Gain = -11.5dB
	1110 1010: Left Channel AGC Gain = -11.0dB
	�
	0000 0000: Left Channel AGC Gain = 0.0dB
	�
	0111 0010: Left Channel AGC Gain = 57.0dB
	0111 0011: Left Channel AGC Gain = 57.5dB
	0111 0100: Left Channel AGC Gain = 58.0dB
*/

#define RCAGCCR1 0,94
/* Right Channel AGC Control Register 1
D7 R/W 0
	0: Right Channel AGC Disabled
	1: Right Channel AGC Enabled
D6-D4 R/W 000 Right Channel AGC Target Level Setting
	000: Right Channel AGC Target Level = -5.5dBFS
	001: Right Channel AGC Target Level = -8.0dBFS
	010: Right Channel AGC Target Level = -10.0dBFS
	011: Right Channel AGC Target Level = -12.0dBFS
	100: Right Channel AGC Target Level = -14.0dBFS
	101: Right Channel AGC Target Level = -17.0dBFS
	110: Right Channel AGC Target Level = -20.0dBFS
	111: Right Channel AGC Target Level = -24.0dBFS
D3-D2 R 00 Reserved. Write only default values
D1-D0 R/W 00 Right Channel AGC Gain Hysteresis Control
	00: Right Channel AGC Gain Hysteresis is disabled
	01: Right Channel AGC Gain Hysteresis is �0.5dB
	10: Right Channel AGC Gain Hysteresis is �1.0dB
	11: Right Channel AGC Gain Hysteresis is �1.5dB
*/

#define RCAGCCR2 0,95
/* Right Channel AGC Control Register 2
D7-D6 R/W 00 Right Channel AGC Hysteresis Setting
	00: Right Channel AGC Hysteresis is 1.0dB
	01: Right Channel AGC Hysteresis is 2.0dB
	10: Right Channel AGC Hysteresis is 4.0dB
	11: Right Channel AGC Hysteresis is disabled
D5-D1 R/W 0 0000 Right Channel AGC Noise Threshold
	0 0000: Right Channel AGC Noise Gate disabled
	0 0001: Right Channel AGC Noise Threshold is -30dB
	0 0010: Right Channel AGC Noise Threshold is -32dB
	0 0011: Right Channel AGC Noise Threshold is -34dB
	�
	1 1101: Right Channel AGC Noise Threshold is -86dB
	1 1110: Right Channel AGC Noise Threshold is -88dB
	1 1111: Right Channel AGC Noise Threshold is -90dB
D0 R 0 Reserved. Write only default value
*/

#define RCAGCCR3 0,96
/* Right Channel AGC Control Register 3
D7 R 0 Reserved. Write only default value
D6-D0 R/W 111 1111 Right Channel AGC Maximum Gain Setting
	000 0000: Right Channel AGC Maximum Gain = 0.0dB
	000 0001: Right Channel AGC Maximum Gain = 0.5dB
	000 0010: Right Channel AGC Maximum Gain = 1.0dB
	�
	111 0011: Right Channel AGC Maximum Gain = 57.5dB
	111 0100: Right Channel AGC Maximum Gain = 58.0dB
	111 0101-111 1111: not recommended for usage, Right Channel AGC Maximum Gain = 58.0dB
*/

#define RCAGCCR4 0,97
/* Right Channel AGC Control Register 4
D7-D3 R/W 0 0000 Right Channel AGC Attack Time Setting
	0 0000: Right Channel AGC Attack Time = 1 * 32 ADC Word Clocks
	0 0001: Right Channel AGC Attack Time = 3 * 32 ADC Word Clocks
	0 0010: Right Channel AGC Attack Time = 5 * 32 ADC Word Clocks
	�
	1 1101: Right Channel AGC Attack Time = 59 * 32 ADC Word Clocks
	1 1110: Right Channel AGC Attack Time = 61 * 32 ADC Word Clocks
	1 1111: Right Channel AGC Attack Time = 63 * 32 ADC Word Clocks
D2-D0 R/W 000 Right Channel AGC Attack Time Scale Factor Setting
	000: Scale Factor = 1
	001: Scale Factor = 2
	010: Scale Factor = 4
	�
	101: Scale Factor = 32
	110: Scale Factor = 64
	111: Scale Factor = 128
*/

#define RCAGCCR5 0,98
/* Right Channel AGC Control Register 5
D7-D3 R/W 0 0000 Right Channel AGC Decay Time Setting
	0 0000: Right Channel AGC Decay Time = 1 * 512 ADC Word Clocks
	0 0001: Right Channel AGC Decay Time = 3 * 512 ADC Word Clocks
	0 0010: Right Channel AGC Decay Time = 5 * 512 ADC Word Clocks
	�
	1 1101: Right Channel AGC Decay Time = 59 * 512 ADC Word Clocks
	1 1110: Right Channel AGC Decay Time = 61 * 512 ADC Word Clocks
	1 1111: Right Channel AGC Decay Time = 63 * 512 ADC Word Clocks
D2-D0 R/W 000 Right Channel AGC Decay Time Scale Factor Setting
	000: Scale Factor = 1
	001: Scale Factor = 2
	010: Scale Factor = 4
	�
	101: Scale Factor = 32
	110: Scale Factor = 64
	111: Scale Factor = 128
*/

#define RCAGCCR6 0,99
/* Right Channel AGC Control Register 6
D7-D5 R 000 Reserved. Write only default values
D4-D0 R/W 0 0000 Right Channel AGC Noise Debounce Time Setting
	0 0001: Right Channel AGC Noise Debounce Time = 0
	0 0010: Right Channel AGC Noise Debounce Time = 4 ADC Word Clocks
	0 0011: Right Channel AGC Noise Debounce Time = 8 ADC Word Clocks
	�
	0 1010: Right Channel AGC Noise Debounce Time = 2048 ADC Word Clocks
	0 1011: Right Channel AGC Noise Debounce Time = 4096 ADC Word Clocks
	0 1100: Right Channel AGC Noise Debounce Time = 2 * 4096 ADC Word Clocks
	0 1101: Right Channel AGC Noise Debounce Time = 3 * 4096 ADC Word Clocks
	...
	1 1101: Right Channel AGC Noise Debounce Time = 19 * 4096 ADC Word Clocks
	1 1110: Right Channel AGC Noise Debounce Time = 20 * 4096 ADC Word Clocks
	1 1111: Right Channel AGC Noise Debounce Time = 21 * 4096 ADC Word Clocks
*/

#define RCAGCCR7 0,100
/* Right Channel AGC Control Register 7
D7-D4 R 0000 Reserved. Write only default values
D3-D0 R/W 0000 Right Channel AGC Signal Debounce Time Setting
	0001: Right Channel AGC Signal Debounce Time = 0
	0010: Right Channel AGC Signal Debounce Time = 4 ADC Word Clocks
	0011: Right Channel AGC Signal Debounce Time = 8 ADC Word Clocks
	�
	1001: Right Channel AGC Signal Debounce Time = 1024 ADC Word Clocks
	1010: Right Channel AGC Signal Debounce Time = 2048 ADC Word Clocks
	1011: Right Channel AGC Signal Debounce Time = 2 * 2048 ADC Word Clocks
	1100: Right Channel AGC Signal Debounce Time = 3 * 2048 ADC Word Clocks
	1101: Right Channel AGC Signal Debounce Time = 4 * 2048 ADC Word Clocks
	1110: Right Channel AGC Signal Debounce Time = 5 * 2048 ADC Word Clocks
	1111: Right Channel AGC Signal Debounce Time = 6 * 2048 ADC Word Clocks
*/

#define RCAGCCR8 0,101
/* Right Channel AGC Control Register 8
D7-D0 R 0000 0000 Right Channel AGC Gain Flag
	1110 1000: Right Channel AGC Gain = -12.0dB
	1110 1001: Right Channel AGC Gain = -11.5dB
	1110 1010: Right Channel AGC Gain = -11.0dB
	�
	0000 0000: Right Channel AGC Gain = 0.0dB
	�
	0111 0010: Right Channel AGC Gain = 57.0dB
	0111 0011: Right Channel AGC Gain = 57.5dB
	0111 0100: Right Channel AGC Gain = 58.0dB
*/

#define DCMR1 0,102
/* DC Measurement Register 1
D7 R/W 0
	0: DC Measurement Mode disabled for Left ADC Channel
	1: DC Measurement Mode enabled for Left ADC Channel
D6 R/W 0
	0: DC Measurement Mode disabled for Right ADC Channel
	1: DC Measurement Mode enabled for Right ADC Channel
D5 R/W 0
	0: DC Measurement is done using 1st order moving average filter with averaging of 2ED
	1: DC Measurement is done with 1sr order Low-pass IIR filter with coefficients as a function of D
D4-D0 R/W 0 0000 DC Measurement D setting
	0 0000: Reserved. Do not use
	0 0001: DC Measurement D parameter = 1
	0 0010: DC Measurement D parameter = 2
	..
	1 0011: DC Measurement D parameter = 19
	1 0100: DC Measurement D parameter = 20
	1 0101-1 1111: Reserved. Do not use
*/

#define DCMR2 0,103
/* DC Measurement Register 2
D7 R 0 Reserved. Write only default values
D6 R/W 0
	0: Left and Right Channel DC measurement result update enabled
	1: Left and Right Channel DC measurement result update disabled; new results will be updated
		while old results are being read
D5 R/W 0
	0: For IIR based DC measurement, measurement value is the instantaneous output of IIR filter
	1: For IIR based DC measurement, the measurement value is updated before periodic clearing of
		IIR filter
D4-D0 R/W 0 0000 IIR based DC Measurement, averaging time setting
	0 0000: Infinite average is used
	0 0001: Averaging time is 2E1 ADC Modulator clocks
	0 0010: Averaging time is 2E2 ADC Modulator clocks
	�
	1 0011: Averaging time is 2E19 ADC Modulator clocks
	1 0100: Averaging time is 2E20 ADC Modulator clocks
	1 0101-1 1111: Reserved. Do not use
*/

#define LCDCMOR1 0,104
/* Left Channel DC Measurement Output Register 1
D7-D0 R 0000 0000 Left Channel DC Measurement Output (23:16)
*/

#define LCDCMOR2 0,105
/* Left Channel DC Measurement Output Register 2
D7-D0 R 0000 0000 Left Channel DC Measurement Output (15:8)
*/

#define LCDCMOR3 0,106
/* Left Channel DC Measurement Output Register 3
D7-D0 R 0000 0000 Left Channel DC Measurement Output (7:0)
*/

#define RCDCMOR1 0,107
/* Right Channel DC Measurement Output Register 1
D7-D0 R 0000 0000 Right Channel DC Measurement Output (23:16)
*/

#define RCDCMOR2 0,108
/* Right Channel DC Measurement Output Register 2
D7-D0 R 0000 0000 Right Channel DC Measurement Output (15:8)
*/

#define RCDCMOR3 0,109
/* Right Channel DC Measurement Output Register 3
D7-D0 R 0000 0000 Right Channel DC Measurement Output (7:0)
*/



// 0 110-127 0x00 0x6E-0x7F Reserved Register


// 1 0 0x01 0x00 Page Select Register

#define PCR 1,1
/* Power Configuration Register
D7-D4 R 0000 Reserved. Write only default values
D3 R/W 0 
	0: AVDD will be weakly connected to DVDD.
		Use when DVDD is powered, but AVDD LDO is
		powered down and AVDD is not externally powered
	1: Disabled weak connection of AVDD with DVDD
D2-D0 R 000 Reserved. Write only default values
*/

#define LDOCR 1,2
/* LDO Control Register
D7-D6 R/W 00 DVDD LDO Control
	00: DVDD LDO output is nominally 1.72V
	01: DVDD LDO output is nominally 1.67V
	10: DVDD LDO output is nominally 1.77V
	11: Do not use
D5-D4 R/W 00 AVDD LDO Control
	00: AVDD LDO output is nominally 1.72V
	01: AVDD LDO output is nominally 1.67V
	10: AVDD LDO output is nominally 1.77V
	11: Do not use
D3 R/W 1 Analog Block Power Control
	0: Analog Blocks Enabled
	1: Analog Blocks Disabled
D2 R 0 DVDD LDO Over Current Detect
	0: Over Current not detected for DVDD LDO
	1: Over Current detected for DVDD LDO
D1 R 0 AVDD LDO Over Current Detect
	0: Over Current not detected for AVDD LDO
	1: Over Current detected for AVDD LDO
D0 R/W 0 AVDD LDO Power Control
	0: AVDD LDO Powered down
	1: AVDD LDO Powered up
*/

#define PCR1 1,3
/* Playback Configuration Register 1
D7-D6 R/W 00
	00: Left DAC routing to HPL uses Class-AB driver
	01-10: Reserved. Do not use
	11: Left DAC routing to HPL uses Class-D driver
D5 R 0 Reserved. Write only default values
D4-D2 R/W 000 Left DAC PTM Control
	000: Left DAC in mode PTM_P3, PTM_P4
	001: Left DAC in mode PTM_P2
	010: Left DAC in mode PTM_P1
	011-111: Reserved. Do not use
D1-D0 R 00 Reserved. Write only default values
*/

#define PCR2 1,4
/* Playback Configuration Register 2
D7-D6 R/W 00
	00: Right DAC routing to HPL uses Class-AB driver
	01-10: Reserved. Do not use
	11: Right DAC routing to HPL uses Class-D driver
D5 R 0 Reserved. Write only default values
D4-D2 R/W 000 Right DAC PTM Control
	000: Right DAC in mode PTM_P3, PTM_P4
	001: Right DAC in mode PTM_P2
	010: Right DAC in mode PTM_P1
	011-111: Reserved. Do not use
D1-D0 R 00 Reserved. Write only default values
*/

// 1 5-8 0x01 0x05-0x08 Reserved Register

#define ODPCR 1,9
/* Output Driver Power Control Register
D7-D6 R 00 Reserved. Write only default value
D5 R/W 0
	0: HPL is powered down
	1: HPL is powered up
D4 R/W 0
	0: HPR is powered down
	1: HPR is powered up
D3 R/W 0
	0: LOL is powered down
	1: LOL is powered up
D2 R/W 0
	0: LOR is powered down
	1: LOR is powered up
D1 R/W 0
	0: Left Mixer Amplifier (MAL) is powered down
	1: Left Mixer Amplifier (MAL) is powered up
D0 R/W 0
	0: Right Mixer Amplifier (MAR) is powered down
	1: Right Mixer Amplifier (MAR) is powered up
*/

#define CMCR 1,10
/* 0x01 0x0A Common Mode Control Register
D7 R 0 Reserved. Write only default value.
D6 R/W 0
	0: Full Chip Common Mode is 0.9V
	1: Full Chip Common Mode is 0.75V
D5-D4 R/W 00
	00: Output Common Mode for HPL and HPR is same as full-chip common mode
	01: Output Common Mode for HPL and HPR is 1.25V
	10: Output Common Mode for HPL and HPR is 1.5V
	11: Output Common Mode for HPL and HPR is 1.65V if D6=0, 1.5V if D6=1
D3 R/W 0
	0: Output Common Mode for LOL and LOR is same as full-chip common mode
	1: Output Common Mode for LOL and LOR is 1.65V and output is powered by LDOIN
D2 R 0 Reserved. Write only default value
D1 R/W 0
	0: Output of HPL and HPR is powered with AVDD supply
	1: Output of HPL and HPR is powered with LDOIN supply
D0 R/W 0
	0: When Page-1, Reg-10, D1 = 1, then LDOIN input range is 1.5V to 1.95V
	1: When Page-1, Reg-10, D1 = 1, then LDOIN input range is 1.8V to 3.6V
*/

#define OCPCR 1,11
/* 0x01 0x0B Over Current Protection Configuration Register
D7-D5 R 000 Reserved. Write only default values
D4 R/W 1
	0: Over Current detection is disabled for HPL and HPR
	1: Over Current detection is enabled for HPL and HPR
D3-D1 R/W 000
	000: No debounce is used for Over Current detection
	001: Over Current detection is debounced by 8ms
	010: Over Current detection is debounce by 16ms
	011: Over Current detection is debounced by 32ms
	100: Over Current detection is debounced by 64ms
	101: Over Current detection is debounced by 128ms
	110: Over Current detection is debounced by 256ms
	111: Over Current detection is debounced by 512ms
D0 R/W 0
	0: Output current will be limited if over current condition is detected
	1: Output driver will be powered down if over current condition is detected
*/

#define HPLRSR 1,12
/* 0x01 0x0C HPL Routing Selection Register
D7-D4 R 0000 Reserved. Write only default values
D3 R/W 0
	0: Left Channel DAC reconstruction filter's positive terminal is not routed to HPL
	1: Left Channel DAC reconstruction filter's positive terminal is routed to HPL
D2 R/W 0
	0: IN1L is not routed to HPL
	1: IN1L is routed to HPL
D1 R/W 0
	0: MAL output is not routed to HPL
	1: MAL output is routed to HPL
D0 R/W 0
	0: MAR output is not routed to HPL
	1: MAR output is routed to HPL
*/

#define HPRRSR 1,13
/* 0x01 0x0D HPR Routing Selection Register
D7-D5 R 000 Reserved. Write only default values
D4 R/W 0
	0: Left Channel DAC reconstruction filter's negative terminal is not routed to HPR
	1: Left Channel DAC reconstruction filter's negative terminal is routed to HPR
D3 R/W 0
	0: Right Channel DAC reconstruction filter's positive terminal is not routed to HPR
	1: Right Channel DAC reconstruction filter's positive terminal is routed to HPR
D2 R/W 0
	0: IN1R is not routed to HPR
	1: IN1R is routed to HPR
D1 R/W 0
	0: MAR output is not routed to HPR
	1: MAR output is routed to HPR
D0 R/W 0
	0: HPL output is not routed to HPR
	1: HPL output is routed to HPR (use when HPL and HPR output is powered by AVDD)
*/

#define LLRSR 1,14
/* 0x01 0x0E LOL Routing Selection Register
D7-D5 R 000 Reserved. Write only default values
D4 R/W 0
	0: Right Channel DAC reconstruction filter's negative terminal is not routed to LOL
	1: Right Channel DAC reconstruction filter's negative terminal is routed to LOL
D3 R/W 0
	0: Left Channel DAC reconstruction filter output is not routed to LOL
	1: Left Channel DAC reconstruction filter output is routed to LOL
D2 R 0 Reserved. Write only default value.
D1 R/W 0
	0: MAL output is not routed to LOL
	1: MAL output is routed to LOL
D0 R/W 0
	0: LOR output is not routed to LOL
	1: LOR output is routed to LOL(use when LOL and LOR output is powered by AVDD)
*/

#define LRRSR 1,15
/* 0x01 0x0F LOR Routing Selection Register
D7-D4 R 0000 Reserved. Write only default values
D3 R/W 0
	0: Right Channel DAC reconstruction filter output is not routed to LOR
	1: Right Channel DAC reconstruction filter output is routed to LOR
D2 R 0 Reserved. Write only default value.
D1 R/W 0
	0: MAR output is not routed to LOR
	1: MAR output is routed to LOR
D0 R 0 Reserved. Write only default value.
*/



#define HPLDGSR 1,16
/* 0x01 0x10 HPL Driver Gain Setting Register
D7 R 0 Reserved. Write only default value.
D6 R/W 1
	0: HPL driver is not muted
	1: HPL driver is muted
D5-D0 R/W 00 0000
	10 0000-11 1001: Reserved. Do not use
	11 1010: HPL driver gain is -6dB
	(Note: It is not possible to mute HPR while programmed to -6dB)
	11 1011: HPL driver gain is -5dB
	11 1100: HPL driver gain is -4dB
	�
	00 0000: HPL driver gain is 0dB
	...
	01 1011: HPL driver gain is 27dB
	01 1100: HPL driver gain is 28dB
	01 1101: HPL driver gain is 29dB
	01 1110-01 1111: Reserved. Do not use
	Note: These gains are not valid while using the driver in Class-D mode
*/

#define HPRDGSR 1,17
/* 0x01 0x11 HPR Driver Gain Setting Register
D7 R 0 Reserved. Write only default value.
D6 R/W 1
	0: HPR driver is not muted
	1: HPR driver is muted
D5-D0 R/W 00 0000
	10 0000-11 1001: Reserved. Do not use
	11 1010: HPR driver gain is -6dB
	(Note: It is not possible to mute HPR while programmed to -6dB)
	11 1011: HPR driver gain is -5dB
	11 1100: HPR driver gain is -4dB
	�
	00 0000: HPR driver gain is 0dB
	...
	01 1011: HPR driver gain is 27dB
	01 1100: HPR driver gain is 28dB
	01 1101: HPR driver gain is 29dB
	01 1110-01 1111: Reserved. Do not use
	Note: These gains are not valid while using the driver in Class-D mode
*/

#define LLDGSR 1,18
/* 0x01 0x12 LOL Driver Gain Setting Register
D7 R 0 Reserved. Write only default value.
D6 R/W 1
	0: LOL driver is not muted
	1: LOL driver is muted
D5-D0 R/W 00 0000
	10 0000-11 1001: Reserved. Do not use
	11 1010: LOL driver gain is -6dB
	11 1011: LOL driver gain is -5dB
	11 1100: LOL driver gain is -4dB
	�
	00 0000: LOL driver gain is 0dB
	...
	01 1011: LOL driver gain is 27dB
	01 1100: LOL driver gain is 28dB
	01 1101: LOL driver gain is 29dB
	01 1110-01 1111: Reserved. Do not use
*/

#define LRDGSR 1,19
/* 0x01 0x13 LOR Driver Gain Setting Register
D7 R 0 Reserved. Write only default value.
D6 R/W 1
	0: LOR driver is not muted
	1: LOR driver is muted
D5-D0 R/W 00 0000
	10 0000-11 1001: Reserved. Do not use
	11 1010: LOR driver gain is -6dB
	11 1011: LOR driver gain is -5dB
	11 1100: LOR driver gain is -4dB
	�
	00 0000: LOR driver gain is 0dB
	...
	01 1011: LOR driver gain is 27dB
	01 1100: LOR driver gain is 28dB
	01 1101: LOR driver gain is 29dB
	01 1110-01 1111: Reserved. Do not use
*/

#define HDSCR 1,20
/* 0x01 0x14 Headphone Driver Startup Control Register
D7-D6 R/W 00
	00: Soft routing step time is 0ms
	01: Soft routing step time is 50ms
	10: Soft routing step time is 100ms
	11: Soft routing step time is 200ms
D5-D2 R/W 0000 0000: Slow power up of headphone amp's is disabled
	0001: Headphone ramps power up slowly in 0.5 time constants
	0010: Headphone ramps power up slowly in 0.625 time constants
	0011; Headphone ramps power up slowly in 0.725 time constants
	0100: Headphone ramps power up slowly in 0.875 time constants
	0101: Headphone ramps power up slowly in 1.0 time constants
	0110: Headphone ramps power up slowly in 2.0 time constants
	0111: Headphone ramps power up slowly in 3.0 time constants
	1000: Headphone ramps power up slowly in 4.0 time constants
	1001: Headphone ramps power up slowly in 5.0 time constants
	1010: Headphone ramps power up slowly in 6.0 time constants
	1011: Headphone ramps power up slowly in 7.0 time constants
	1100: Headphone ramps power up slowly in 8.0 time constants
	1101: Headphone ramps power up slowly in 16.0 time constants ( do not use for Rchg=25K)
	1110: Headphone ramps power up slowly in 24.0 time constants (do not use for Rchg=25K)
	1111: Headphone ramps power up slowly in 32.0 time constants (do not use for Rchg=25K)
	Note: Time constants assume 47uF decoupling cap
D1-D0 R/W 00 
	00: Headphone ramps power up time is determined with 25k resistance
	01: Headphone ramps power up time is determined with 6k resistance
	10: Headphone ramps power up time is determined with 2k resistance
	11: Reserved. Do not use
*/

// 1 21 0x01 0x15 Reserved Register

#define HPLVCR 1,22
/* 0x01 0x16 IN1L to HPL Volume Control Register
D7 R 0 Reserved. Write only default value.
D6-D0 R/W 000 0000 IN1L to HPL Volume Control
	000 0000: Volume Control = 0.0dB
	000 0001: Volume Control = -0.5dB
	000 0010: Volume Control = -1.0dB
	000 0011: Volume Control = -1.5dB
	000 0100: Volume Control = -2.0dB
	000 0101: Volume Control = -2.5dB
	000 0110: Volume Control = -3.0dB
	000 0111: Volume Control = -3.5dB
	000 1000: Volume Control = -4.0dB
	000 1001: Volume Control = -4.5dB
	000 1010: Volume Control = -5.0dB
	000 1011: Volume Control = -5.5dB
	000 1100: Volume Control = -6.0dB
	000 1101: Volume Control = -6.5dB
	000 1110: Volume Control = -7.0dB
	000 1111: Volume Control = -7.5dB
	001 0000: Volume Control = -8.0dB
	001 0001: Volume Control = -8.5dB
	001 0010: Volume Control = -9.0dB
	001 0011: Volume Control = -9.5dB
	001 0100: Volume Control = -10.0dB
	001 0101: Volume Control = -10.5dB
	001 0110: Volume Control = -11.0dB
	001 0111: Volume Control = -11.5dB
	001 1000: Volume Control = -12.0dB
	001 1001: Volume Control = -12.5dB
	001 1010: Volume Control = -13.0dB
	001 1011: Volume Control = -13.5dB
	001 1100: Volume Control = -14.0dB
	001 1101: Volume Control = -14.5dB
	001 1110: Volume Control = -15.0dB
	001 1111: Volume Control = -15.5dB
	010 0000: Volume Control = -16.0dB
	010 0001: Volume Control = -16.5dB
	010 0010: Volume Control = -17.1dB
	010 0011: Volume Control = -17.5dB
	010 0100: Volume Control = -18.1dB
	010 0101: Volume Control = -18.6dB
	010 0110: Volume Control = -19.1dB
	010 0111: Volume Control = -19.6dB
	010 1000: Volume Control = -20.1dB
	010 1001: Volume Control = -20.6dB
	010 1010: Volume Control = -21.1dB
	010 1011: Volume Control = -21.6dB
	010 1100: Volume Control = -22.1dB
	010 1101: Volume Control = -22.6dB
	010 1110: Volume Control = -23.1dB
	010 1111: Volume Control = -23.6dB
	011 0000: Volume Control = -24.1dB
	011 0001: Volume Control = -24.6dB
	011 0010: Volume Control = -25.1dB
	011 0011: Volume Control = -25.6dB
	011 0100: Volume Control = -26.1dB
	011 0101: Volume Control = -26.6dB
	011 0110: Volume Control = -27.1dB
	011 0111: Volume Control = -27.6dB
	011 1000: Volume Control = -28.1dB
	011 1001: Volume Control = -28.6dB
	011 1010: Volume Control = -29.1dB
	011 1011: Volume Control = -29.6dB
	011 1100: Volume Control = -30.1dB
	011 1101: Volume Control = -30.6dB
	011 1110: Volume Control = -31.1dB
	011 1111: Volume Control = -31.6dB
	100 0000: Volume Control = -32.1dB
	100 0001:Volume Control = -32.6dB
	100 0010: Volume Control = -33.1dB
	100 0011: Volume Control = -33.6dB
	100 0100: Volume Control = -34.1dB
	100 0101: Volume Control = -34.6dB
	100 0110: Volume Control = -35.2dB
	100 0111: Volume Control = -35.7dB
	100 1000: Volume Control = -36.2dB
	100 1001: Volume Control = -36.7dB
	100 1010: Volume Control = -37.2dB
	100 1011:Volume Control = -37.7dB
	100 1100: Volume Control = -38.2dB
	100 1101: Volume Control = -38.7dB
	100 1110: Volume Control = -39.2dB
	100 1111: Volume Control = -39.7dB
	101 0000: Volume Control = -40.2dB
	101 0001: Volume Control = -40.7dB
	101 0010: Volume Control = -41.2dB
	101 0011: Volume Control = -41.7dB
	101 0100: Volume Control = -42.1dB
	101 0101: Volume Control = -42.7dB
	101 0110: Volume Control = -43.2dB
	101 0111: Volume Control = -43.8dB
	101 1000: Volume Control = -44.3dB
	101 1001: Volume Control = -44.8dB
	101 1010: Volume Control = -45.2dB
	101 1011: Volume Control = -45.8dB
	101 1100: Volume Control = -46.2dB
	101 1101: Volume Control = -46.7dB
	101 1110: Volume Control = -47.4dB
	101 1111: Volume Control = -47.9dB
	110 0000: Volume Control = -48.2dB
	110 0001: Volume Control = -48.7dB
	110 0010: Volume Control = -49.3dB
	110 0011: Volume Control = -50.0dB
	110 0100: Volume Control = -50.3dB
	110 0101: Volume Control = -51.0dB
	110 0110: Volume Control = -51.4dB
	110 0111: Volume Control = -51.8dB
	110 1000: Volume Control = -52.3dB
	110 1001: Volume Control = -52.7dB
	110 1010: Volume Control = -53.7dB
	110 1011: Volume Control = -54.2dB
	110 1100: Volume Control = -55.4dB
	110 1101: Volume Control = -56.7dB
	110 1110: Volume Control = -58.3dB
	110 1111: Volume Control = -60.2dB
	111 0000: Volume Control = -62.7dB
	111 0001: Volume Control = -64.3dB
	111 0010: Volume Control = -66.2dB
	111 0011: Volume Control = -68.7dB
	111 0100: Volume Control = -72.3dB
	111 0101: Volume Control = MUTE
	111 0110-111 1111: Reserved. Do not use
*/

#define HPRVCR 1,23
/* 0x01 0x17 IN1R to HPR Volume Control Register
	*See HPLVCR above
*/

#define MALVCR 1,24
/* 0x01 0x18 Mixer Amplifier Left Volume Control Register
D7-D6 R 00 Reserved. Write only default values
D5-D0 R/W 00 0000 Mixer Amplifier Left Volume Control
	00 0000: Volume Control = 0.0dB
	00 0001: Volume Control = -0.4dB
	00 0010: Volume Control = -0.9dB
	00 0011: Volume Control = -1.3dB
	00 0100: Volume Control = -1.8dB
	00 0101: Volume Control = -2.3dB
	00 0110: Volume Control = -2.9dB
	00 0111: Volume Control = -3.3dB
	00 1000: Volume Control = -3.9dB
	00 1001: Volume Control = -4.3dB
	00 1010: Volume Control = -4.8dB
	00 1011: Volume Control = -5.2dB
	00 1100: Volume Control = -5.8dB
	00 1101: Volume Control = -6.3dB
	00 1110: Volume Control = -6.6dB
	00 1111: Volume Control = -7.2dB
	01 0000: Volume Control = -7.8dB
	01 0001: Volume Control = -8.2dB
	01 0010: Volume Control = -8.5dB
	01 0011: Volume Control = -9.3dB
	01 0100: Volume Control = -9.7dB
	01 0101: Volume Control = -10.1dB
	01 0110: Volume Control = -10.6dB
	01 0111: Volume Control = -11.0dB
	01 1000: Volume Control = -11.5dB
	01 1001: Volume Control = -12.0dB
	01 1010: Volume Control = -12.6dB
	01 1011: Volume Control = -13.2dB
	01 1100: Volume Control = -13.8dB
	01 1101: Volume Control = -14.5dB
	01 1110: Volume Control = -15.3dB
	01 1111: Volume Control = -16.1dB
	10 0000: Volume Control = -17.0dB
	10 0001: Volume Control = -18.1dB
	10 0010: Volume Control = -19.2dB
	10 0011: Volume Control = -20.6dB
	10 0100: Volume Control = -22.1dB
	10 0101: Volume Control = -24.1dB
	10 0110: Volume Control = -26.6dB
	10 0111: Volume Control = -30.1dB
	10 1000: Volume Control = MUTE
	10 1001-11 1111: Reserved. Do no use
*/

#define MARVCR 1,25
/* 0x01 0x19 Mixer Amplifier Right Volume Control Register
	*See MALVCR above
*/

// 1 26-50 0x01 0x1A-0x32 Reserved Register

#define MICBIASCR 1,51
/* 0x01 0x33 MICBIAS Configuration Register
D7 R 0 Reserved. Write only default value.
D6 R/W 0
	0: MICBIAS powered down
	1: MICBIAS powered up
D5-D4 R/W 00 MICBIAS Output Voltage Configuration
	00: MICBIAS = 1.04V (CM = 0.75V) or MICBIAS = 1.25V(CM = 0.9V)
	01: MICBIAS = 1.425V(CM = 0.75V) or MICBIAS = 1.7V(CM = 0.9V)
	10: MICBIAS = 2.075V(CM = 0.75V) or MICBIAS = 2.5V(CM = 0.9V)
	11: MICBIAS is switch to power supply
D3 R/W 0
	0: MICBIAS voltage is generated from AVDD
	1: MICBIAS voltage is generated from LDOIN
D2-D0 R 000 Reserved. Write only default value.
*/

#define LMPPTIRCR 1,52
/* 0x01 0x34 Left MICPGA Positive Terminal Input Routing Configuration Register
D7-D6 R/W 00 IN1L to Left MICPGA positive terminal selection
	00: IN1L is not routed to Left MICPGA
	01: IN1L is routed to Left MICPGA with 10k resistance
	10: IN1L is routed to Left MICPGA with 20k resistance
	11: IN1L is routed to Left MICPGA with 40k resistance
D5-D4 R/W 00 IN2L to Left MICPGA positive terminal selection
	00: IN2L is not routed to Left MICPGA
	01: IN2L is routed to Left MICPGA with 10k resistance
	10: IN2L is routed to Left MICPGA with 20k resistance
	11: IN2L is routed to Left MICPGA with 40k resistance
D3-D2 R/W 00 IN3L to Left MICPGA positive terminal selection
	00: IN3L is not routed to Left MICPGA
	01: IN3L is routed to Left MICPGA with 10k resistance
	10: IN3L is routed to Left MICPGA with 20k resistance
	11: IN3L is routed to Left MICPGA with 40k resistance
D1-D0 R/W 00 IN1R to Left MICPGA positive terminal selection
	00: IN1R is not routed to Left MICPGA
	01: IN1R is routed to Left MICPGA with 10k resistance
	10: IN1R is routed to Left MICPGA with 20k resistance
	11: IN1R is routed to Left MICPGA with 40k resistance
*/

// 1 53 0x01 0x35 Reserved Register

#define LMPNTIRCR 1,54
/* 0x01 0x36 Left MICPGA Negative Terminal Input Routing Configuration Register
D7-D6 R/W 00 CM to Left MICPGA (CM1L) positive terminal selection
	00: CM is not routed to Left MICPGA
	01: CM is routed to Left MICPGA via CM1L with 10k resistance
	10: CM is routed to Left MICPGA via CM1L with 20k resistance
	11: CM is routed to Left MICPGA via CM1L with 40k resistance
D5-D4 R/W 00 IN2R to Left MICPGA positive terminal selection
	00: IN2R is not routed to Left MICPGA
	01: IN2R is routed to Left MICPGA with 10k resistance
	10: IN2R is routed to Left MICPGA with 20k resistance
	11: IN2R is routed to Left MICPGA with 40k resistance
D3-D2 R/W 00 IN3R to Left MICPGA positive terminal selection
	00: IN3R is not routed to Left MICPGA
	01: IN3R is routed to Left MICPGA with 10k resistance
	10: IN3R is routed to Left MICPGA with 20k resistance
	11: IN3R is routed to Left MICPGA with 40k resistance
D1-D0 R/W 00 CM to Left MICPGA (CM2L) positive terminal selection
	00: CM is not routed to Left MICPGA
	01: CM is routed to Left MICPGA via CM2L with 10k resistance
	10: CM is routed to Left MICPGA via CM2L with 20k resistance
	11: CM is routed to Left MICPGA via CM2L with 40k resistance
*/

#define RMPPTIRCR 1,55
/* 0x01 0x37 Right MICPGA Positive Terminal Input Routing Configuration Register
D7-D6 R/W 00 IN1R to Right MICPGA positive terminal selection
	00: IN1R is not routed to Right MICPGA
	01: IN1R is routed to Right MICPGA with 10k resistance
	10: IN1R is routed to Right MICPGA with 20k resistance
	11: IN1R is routed to Right MICPGA with 40k resistance
D5-D4 R/W 00 IN2R to Right MICPGA positive terminal selection
	00: IN2R is not routed to Right MICPGA
	01: IN2R is routed to Right MICPGA with 10k resistance
	10: IN2R is routed to Right MICPGA with 20k resistance
	11: IN2R is routed to Right MICPGA with 40k resistance
D3-D2 R/W 00 IN3R to Right MICPGA positive terminal selection
	00: IN3R is not routed to Right MICPGA
	01: IN3R is routed to Right MICPGA with 10k resistance
	10: IN3R is routed to Right MICPGA with 20k resistance
	11: IN3R is routed to Right MICPGA with 40k resistance
D1-D0 R/W 00 IN2L to Right MICPGA positive terminal selection
	00: IN2L is not routed to Right MICPGA
	01: IN2L is routed to Right MICPGA with 10k resistance
	10: IN2L is routed to Right MICPGA with 20k resistance
	11: IN2L is routed to Right MICPGA with 40k resistance
*/

// 1 56 0x01 0x38 Reserved Register

#define RMPNTIRCR 1,57
/* 0x01 0x39 Right MICPGA Negative Terminal Input Routing Configuration Register
D7-D6 R/W 00 CM to Right MICPGA (CM1R) positive terminal selection
	00: CM is not routed to Right MICPGA
	01: CM is routed to Right MICPGA via CM1R with 10k resistance
	10: CM is routed to Right MICPGA via CM1R with 20k resistance
	11: CM is routed to Right MICPGA via CM1R with 40k resistance
D5-D4 R/W 00 IN1L to Right MICPGA positive terminal selection
	00: IN1L is not routed to Right MICPGA
	01: IN1L is routed to Right MICPGA with 10k resistance
	10: IN1L is routed to Right MICPGA with 20k resistance
	11: IN1L is routed to Right MICPGA with 40k resistance
D3-D2 R/W 00 IN3L to Right MICPGA positive terminal selection
	00: IN3L is not routed to Right MICPGA
	01: IN3L is routed to Right MICPGA with 10k resistance
	10: IN3L is routed to Right MICPGA with 20k resistance
	11: IN3L is routed to Right MICPGA with 40k resistance
D1-D0 R/W 00 CM to Right MICPGA (CM2R) positive terminal selection
	00: CM is not routed to Right MICPGA
	01: CM is routed to Right MICPGA via CM2R with 10k resistance
	10: CM is routed to Right MICPGA via CM2R with 20k resistance
	11: CM is routed to Right MICPGA via CM2R with 40k resistance
*/

#define FICR 1,58
/* 0x01 0x3A Floating Input Configuration Register
D7 R/W 0
	0: IN1L input is not weakly connected to common mode
	1: IN1L input is weakly driven to common mode. Use when not routing IN1L to Left and Right
		MICPGA and HPL
D6 R/W 0
	0: IN1R input is not weakly connected to common mode
	1: IN1R input is weakly driven to common mode. Use when not routing IN1L to Left and Right
		MICPGA and HPR
D5 R/W 0
	0: IN2L input is not weakly connected to common mode
	1: IN2L input is weakly driven to common mode. Use when not routing IN2L to Left and Right
MICPGA
D4 R/W 0
	0: IN2R input is not weakly connected to common mode
	1: IN2R input is weakly driven to common mode. Use when not routing IN2R to Left and Right
MICPGA
D3 R/W 0
	0: IN3L input is not weakly connected to common mode
	1: IN3L input is weakly driven to common mode. Use when not routing IN3L to Left and Right
MICPGA
D2 R/W 0
	0: IN3R input is not weakly connected to common mode
	1: IN3R input is weakly driven to common mode. Use when not routing IN3R to Left and Right
MICPGA
D1-D0 R 00 Reserved. Write only default values
*/

#define LMPVCR 1,59
/* 0x01 0x3B Left MICPGA Volume Control Register
D7 R/W 1
	0: Left MICPGA Gain is enabled
	1: Left MICPGA Gain is set to 0dB
D6-D0 R/W 000 0000 Left MICPGA Volume Control
	000 0000: Volume Control = 0.0dB
	000 0001: Volume Control = 0.5dB
	000 0010: Volume Control = 1.0dB
	�
	101 1101: Volume Control = 46.5dB
	101 1110: Volume Control = 47.0dB
	101 1111: Volume Control = 47.5dB
	110 0000-111 1111: Reserved. Do not use
*/

#define RMPVCR 1,60
/* 0x01 0x3C Right MICPGA Volume Control Register
	* See LMPVCR, replace Left with Right as appropriate.
*/

#define ADCPTCR 1,61
/* 0x01 0x3D ADC Power Tune Configuration Register
D7-D0 R/W 0000 0000
	0000 0000: PTM_R4 (Default)
	0110 0100: PTM_R3
	1011 0110: PTM_R2
	1111 1111: PTM_R1
*/

#define ADCAVCFR 1,62
/* 0x01 0x3E ADC Analog Volume Control Flag Register
D1 R 0 Left Channel Analog Volume Control Flag
	0: Applied Volume is not equal to Programmed Volume
	1: Applied Volume is equal to Programmed Volume
D0 R 0 Right Channel Analog Volume Control Flag
	0: Applied Volume is not equal to Programmed Volume
	1: Applied Volume is equal to Programmed Volume
*/

#define DAGCFR 1,63
/* 0x01 0x3F DAC Analog Gain Control Flag Register
D7 R 0 HPL Gain Flag
	0: Applied Gain is not equal to Programmed Gain
	1: Applied Gain is equal to Programmed Gain
D6 R 0 HPR Gain Flag
	0: Applied Gain is not equal to Programmed Gain
	1: Applied Gain is equal to Programmed Gain
D5 R 0 LOL Gain Flag
	0: Applied Gain is not equal to Programmed Gain
	1: Applied Gain is equal to Programmed Gain
D4 R 0 LOR Gain Flag
	0: Applied Gain is not equal to Programmed Gain
	1: Applied Gain is equal to Programmed Gain
D3 R 0 IN1L to HPL Bypass Volume Flag
	0: Applied Volume is not equal to Programmed Volume
	1: Applied Volume is equal to Programmed Volume
D2 R 0 IN1R to HPR Bypass Volume Flag
	0: Applied Volume is not equal to Programmed Volume
	1: Applied Volume is equal to Programmed Volume
D1 R 0 MAL Volume Flag
	0: Applied Volume is not equal to Programmed Volume
	1: Applied Volume is equal to Programmed Volume
D0 R 0 MAR Volume Flag
	0: Applied Volume is not equal to Programmed Volume
	1: Applied Volume is equal to Programmed Volume
*/

// 1 64-70 0x01 0x40-0x46 Reserved Register

#define AIQCCR 1,71
/* 0x01 0x47 Analog Input Quick Charging Configuration Register
D7-D6 R 00 Reserved. Write only default values
D5-D0 R/W 00 0000 Analog inputs power up time
	00 0000: Default. Use one of the values give below
	11 0001: Analog inputs power up time is 3.1 ms
	11 0010: Analog inputs power up time is 6.4 ms
	11 0011: Analog inputs power up time is 1.6 ms
	Others: Do not use
*/

// 1 72-122 0x01 0x48-0x7A Reserved Register

#define RPCR 1,123
/* 0x01 0x7B Reference Power-up Configuration Register
D7-D3 R 0 0000 Reserved. Write only default values
D2-D0 R/W 000 Reference Power Up configuration
	000: Reference will power up slowly when analog blocks are powered up
	001: Reference will power up in 40ms when analog blocks are powered up
	010: Reference will power up in 80ms when analog blocks are powered up
	011: Reference will power up in 120ms when analog blocks are powered up
	100: Force power up of reference. Power up will be slow
	101: Force power up of reference. Power up time will be 40ms
	110: Force power up of reference. Power up time will be 80ms
	111: Force power up of reference. Power up time will be 120ms

*/

// 1 124-127 0x01 0x7C-0x7F Reserved Register

// 8 0 0x08 0x00 Page Select Register

#define AAFCR 8,1
/* 0x08 0x01 ADC Adaptive Filter Configuration Register
D7-D3 R 0000 0 Reserved. Write only default values
D2 R/W 0 ADC Adaptive Filtering Control
	0: Adaptive Filtering disabled for ADC
	1: Adaptive Filtering enabled for ADC
D1 R 0 ADC Adaptive Filter Buffer Control Flag
	0: In adaptive filter mode, ADC accesses ADC Coefficient Buffer-A and control interface accesses
		ADC Coefficient Buffer-B
	1: In adaptive filter mode, ADC accesses ADC Coefficient Buffer-B and control interface accesses
		ADC Coefficient Buffer-A
D0 R/W 0 ADC Adaptive Filter Buffer Switch control
	0: ADC Coefficient Buffers will not be switched at next frame boundary
	1: ADC Coefficient Buffers will be switched at next frame boundary, if in adaptive filtering mode.
This will self clear on switching.
*/

// 8 2-7 0x08 0x02-0x07 Reserved

// 8 8-127 0x08 0x08-0x7F ADC Coefficients Buffer-A C(0:29)

// 9-16 0 0x09-0x10 0x00 Page Select Register

// 9-16 1-7 0x09-0x10 0x01-0x07 Reserved

// 9-16 8-127 0x09-0x10 0x08-0x7F ADC Coefficients Buffer-A C(30:255)

// 26-34 0 0x1A-0x22 0x00 Page Select Register

// 26-34 1-7 0x1A-0x22 0x01-0x07 Reserved.

// 26-34 8-127 0x1A-0x22 0x08-0x7F ADC Coefficients Buffer-B C(0:255)

// 44 0 0x2C 0x00 Page Select Register

#define DAFCR 44,1
/* 0x2C 0x01 DAC Adaptive Filter Configuration Register
D7-D3 R 0000 0 Reserved. Write only default values
D2 R/W 0 DAC Adaptive Filtering Control
	0: Adaptive Filtering disabled for DAC
	1: Adaptive Filtering enabled for DAC
D1 R 0 DAC Adaptive Filter Buffer Control Flag
	0: In adaptive filter mode, DAC accesses DAC Coefficient Buffer-A and control interface accesses
		DAC Coefficient Buffer-B
	1: In adaptive filter mode, DAC accesses DAC Coefficient Buffer-B and control interface accesses
		DAC Coefficient Buffer-A
D0 R/W 0 DAC Adaptive Filter Buffer Switch control
	0: DAC Coefficient Buffers will not be switched at next frame boundary
	1: DAC Coefficient Buffers will be switched at next frame boundary, if in adaptive filtering mode.
		This will self clear on switching.
*/

// 44 2-7 0x2C 0x02-0x07 Reserved

// 44 8-127 0x2C 0x08-0x7F DAC Coefficients Buffer-A C(0:29)

// 45-52 0 0x2D-0x34 0x00 Page Select Register

// 45-52 1-7 0x2D-0x34 0x01-0x07 Reserved.

// 45-52 8-127 0x2D-0x34 0x08-0x7F DAC Coefficients Buffer-A C(30:255)

// 62-70 0 0x3E-0x46 0x00 Page Select Register

// 62-70 1-7 0x3E-0x46 0x01-0x07 Reserved.

// 62-70 8-127 0x3E-0x46 0x08-0x7F DAC Coefficients Buffer-B C(0:255)

// 80-114 0 0x50-0x72 0x00 Page Select Register

// 80-114 1-7 0x50-0x72 0x01-0x07 Reserved.

// 80-114 8-127 0x50-0x72 0x08-0x7F miniDSP_A Instructions

//152-186 0 0x98-0xBA 0x00 Page Select Register

// 152-186 1-7 0x98-0xBA 0x01-0x07 Reserved.

// 152-186 8-127 0x98-0xBA 0x08-0x7F miniDSP_D Instructions


