
#include "blender.h"
#include "utility/orgasm.h" 
/*	void gasmInt2Float(int16_t* ptr16, float* ptrf);
	void gasmInt2FloatPeak(int16_t* ptr16, float* ptrf, float* peaks);
	void gasmFloat2Int(float* ptrf, int16_t* ptr16);
	void gasmFloat2IntGain(float* ptrf, int16_t* ptr16, float* gain);
	void gasmFloat2IntGainPeak(float* ptrf, int16_t* ptr16, float* gain, float* peak);
	void gasmBiquadf(float* ptrf, float* coefs);
	void gasmBiquadff(float* dest, float* coefs, float* src);
	void gasmMix2(float* buff0, float* buff1, float* gain);

	void gasmCaterPeaks(float* peakinfoin, float* peakinfoout, float divisor=65535.0); */

	
/* Entire Audio Pathway as if described using PJRC Audio Objects
 /-----------\
 |           |
 |   i2s_In  |   muxer
 |  /------\ | /-------\    mixb0
 |  |      | | |       |   /-----\    filt0     mixb1
 |  | in0 *=-^-=*      |   |     |   /-----\   /-----\    filt1     mix_v
 |  |      |   |  CHO *=---=*    |   |     |   |     |   /-----\   /-----\    i2s_Out
 |  | in1 *=---=*      |   |    *=---=*   *=---=*    |   |     |   |     |   /-------\
 |  |      |   |  CH1 *=---=*    |   |     |   |    *=---=*   *=-v-=*    |   |       |
 |  | in2 *=---=*      |   |     |   \-----/ /-=*    |   |     | | |    *=---=* out0 |
 |  |      |   |  CH2 *=-\ =*    |           | |     |   \-----/ | =*    |   |       |
 |  | in3 *=---=*      | | |     |    pass1  | =*    |           | |     | /-=* out1 |
 |  |      |   |       | | =*    |   /-----\ | |     | /---------/ =*    | | |       |
 |  \------/   \-------/ | |     |   |     | | =*    | |           |     | | =* out2 |
 |                       | \-----/ /-=*   *=-/ |     | |  peak0    =*    | | |       |
 |    filtt              |         | |     |   \-----/ | /-----\   |     | | =* out3 |
 |   /-----\             |  pass0  | \-----/           \-=*    |   \-----/ | |       |
 |   |     |             | /-----\ |                     \-----/           | \-------/
 \---=*   *=-\           | |     | |       peakt                    mixt   |
     |     | |           \-=*   *=-/      /-----\                  /-----\ |
     \-----/ |             |     |     /--=*    |                  |     | |
             |             \-----/     |  \-----/               /--=*    | |
             |                         |                        |  |    *=-/
             \-------------------------^------------------------/  =*    |
                                                                   |     |
                                                                   =*    |
                                                                   |     |
                                                                   =*    |
                                                                   |     |
                                                                   \-----/
This would give 7*(128/48000) seconds latency - the code below does everything between the two i2s objects in 1*(128/48000) seconds instead. */
	

void EiconPreamp::update(void)
{
	audio_block_t *in[4]={NULL,NULL,NULL,NULL},*out0=NULL,*out1=NULL;
	int16_t *ch0ptr=NULL,*ch1ptr=NULL,*ch2ptr=NULL,*out0ptr=NULL,*out1ptr=NULL /*,*end0ptr=NULL*/;
	uint8_t n=0;
	
	in[0]=receiveReadOnly(0);
	in[1]=receiveReadOnly(1);
	in[2]=receiveReadOnly(2);
	in[3]=receiveReadOnly(3);
	
	out0=allocate();
	if(!out0)
	{
		if(in[0]) release(in[0]);
		if(in[1]) release(in[1]);
		if(in[2]) release(in[2]);
		if(in[3]) release(in[3]);
		
		return; // if I cannot allocate out0 then I am too annoyed to proceed...
	}

	out1=allocate();

/*	if(inCon==NULL) - tho, not really possible to instantiate without having one of these.
	{
		if(in[0]) ch0ptr=in[0]->data; else ch0ptr=&silence[0];
		if(in[1]) ch0ptr=in[0]->data; else ch1ptr=&silence[0];
		if(in[2]) ch0ptr=in[0]->data; else ch2ptr=&silence[0];
	} else { */
		n=inCon->channel[0]&3;
		if(((inCon->channel[0]&32)==32)&&(in[n]!=NULL)) ch0ptr=in[n]->data; else ch0ptr=&silence[0];
		n=inCon->channel[1]&3;
		if(((inCon->channel[1]&32)==32)&&(in[n]!=NULL)) ch1ptr=in[n]->data; else ch1ptr=&silence[0];
		n=inCon->channel[2]&3;
		if(((inCon->channel[2]&32)==32)&&(in[n]!=NULL)) ch2ptr=in[n]->data; else ch2ptr=&silence[0];
//	}
	out0ptr=out0->data;
	if(out1) out1ptr=out1->data; else out1ptr=out0ptr; // just gets burnt at the end of the do {} if pointing at out0.
	
	
	gasmInt2FloatPeak(ch0ptr, fbuf0, &peaks.ch0.peaklo); // take peak reading of ch0 and convert to float into fbuf0
	
#if 1
	gasmBiquadff(fbuf1, tunerfilters, fbuf0);
	gasmFloat2IntGainPeak(fbuf1, out1ptr, &out1gain, &peaks.tune.peaklo); // the tuner output is ready in out1 and we took the peak measurement before applying gain. (peak after filters...)
#else
	gasmBiquadffPeak(fbuf1, tunerfilters, fbuf0, &peaks.tune.peaklo); // peak before filters...
	gasmFloat2IntGain(fbuf1, out1ptr, &out1gain); // the tuner output is ready in out1
#endif


	
	gasmInt2FloatPeak(ch1ptr, fbuf1, &peaks.ch1.peaklo);
	
	gasmBiquadf(fbuf0, ch0filters);
	gasmBiquadf(fbuf1, ch1filters);

	gasmMix2(fbuf0, fbuf1, blend0gain);
	
	gasmInt2FloatPeak(ch2ptr, fbuf1, &peaks.ch2.peaklo);
	
	gasmBiquadf(fbuf1, ch2filters);
	gasmBiquadf(fbuf0, blend1filter);

	gasmMix2(fbuf0, fbuf1, blend1gain); // we just lost interest in fbuf1 for this pass.
	
	gasmBiquadf(fbuf0, tonefilters);
	
	gasmFloat2IntGainPeak(fbuf0, out0ptr, &out0gain, &peaks.out.peaklo); // final output is ready in out0
	
	gasmCaterPeaksAndCounters(&peaks.root.peaklo, &counters[0]);
	
	transmit(out0,0);
	if(out1)
	{
		transmit(out1,1);
		release(out1);
	} else {
		transmit(out0,1);
	}
	release(out0);
// Would I like to offer any input as a 3rd output of this object?
	if(in[0]) release(in[0]);
	if(in[1]) release(in[1]);
	if(in[2]) release(in[2]);
	if(in[3]) release(in[3]);
}

/* Some folks would disable interrupts before even reading some of the values I read below, let alone before setting or re-setting them.
		Those folks can bite me :D */

	void EiconPreamp::setVolume(float newVal) { out0gain=newVal; }
	void EiconPreamp::setTunerGain(float newVal) { out1gain=newVal; }
	void EiconPreamp::setBlend0Gain(uint8_t n, float m) { blend0gain[n]=m; }
	void EiconPreamp::setBlend1Gain(uint8_t n, float m) { blend1gain[n]=m; }
	
	uint8_t EiconPreamp::counter(uint8_t n, bool reset)
	{
		if(!n) return 0;
		uint8_t m=counters[n];
		if(reset) counters[n]=0;
		return m;
	}
	
	float EiconPreamp::peakCh0(bool reset)
	{
		float n=peaks.ch0.peak;
		if(reset) // some would more than suggest I should disable interrupts before touching these
		{
			peaks.ch0.peaklo=32767;
			peaks.ch0.peakhi=-32768;
			peaks.ch0.peak=0;
		} // I cordially invite them to bite me :)
		return n;
	}
	
	float EiconPreamp::peakCh1(bool reset)
	{
		float n=peaks.ch1.peak;
		if(reset)
		{
			peaks.ch1.peaklo=32767;
			peaks.ch1.peakhi=-32768;
			peaks.ch1.peak=0;
		}
		return n;
	}
	
	float EiconPreamp::peakCh2(bool reset)
	{
		float n=peaks.ch2.peak;
		if(reset)
		{
			peaks.ch2.peaklo=32767;
			peaks.ch2.peakhi=-32768;
			peaks.ch2.peak=0;
		}
		return n;
	}

	float EiconPreamp::peakOut(bool reset)
	{
		float n=peaks.out.peak;
		if(reset)
		{
			peaks.out.peaklo=32767;
			peaks.out.peakhi=-32768;
			peaks.out.peak=0;
		}
		return n;
	}

	float EiconPreamp::peakTune(bool reset)
	{
		float n=peaks.tune.peak;
		if(reset)
		{
			peaks.tune.peaklo=32767;
			peaks.tune.peakhi=-32768;
			peaks.tune.peak=0;
		}
		return n;
	}

