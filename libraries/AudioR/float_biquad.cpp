
#include "float_biquad.h"
#include "utility/fasterBiquad.h"

void AudioFloatBiquad::update(void)
{
	audio_block_t *block;
	
	block = receiveWritable();
	if (!block) return;

	if(definition) suckBiquads(block->data, definition);
	
	transmit(block);
	release(block);
}
	