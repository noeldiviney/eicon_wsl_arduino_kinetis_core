/* Objective: Tuner works from samples */



#ifndef _ANALYSE_TUNER_H_
#define _ANALYSE_TUNER_H_

#include "Arduino.h"
#include "AudioStream48.h"

#define TUNAMINIMUMFREQUENCY 20
#define TUNAINTERVALTHRESHOLD (1.0f/TUNAMINIMUMFREQUENCY)

#define TUNAFLAGVARTYPE uint16_t
#define TUNAPRECISIONHI double
#define TUNAPRECISIONLO float

/* uint16_t tuna_flags=0x0; - externally compatible with out tuner, per se.

// for the sake of some compatibility these flags are in the same place as previous versions.

[F] TUNAFLASHTUNA
[EDCB] unused.
[A] TUNAFLAGTIGHT
[9] TUNAFLAGTUNED
[8] TUNAFLAGLOOSE
[7] TUNAFLAGDTCTG
[654] unused.
[3210] TUNAFLAGNOTENUMBER

*/

#define TUNAFRESHTUNA 0x8000
#define TUNAFLAGTIGHT 0x400
#define TUNAFLAGTUNED 0x200
#define TUNAFLAGLOOSE 0x100
#define TUNAFLAGDTCTG 0x80
#define TUNAFLAGNOTED 0xF

// this is the '-' at the end of the note list.
#define TUNAFLAGQUIET 12

struct dp_t {
	int16_t data=0;
	uint16_t count=0;
	int8_t dir=1;
};

/* struct { toying with potentially silly idea.
	TUNAFLAGVARTYPE flags=TUNAFRESHTUNA|TUNAFLAGTUNED|TUNAFLAGLOOSE|TUNAFLAGTIGHT|TUNAFLAGQUIET;
	
	TUNAPRECISIONHI toleranceSampling=0.025;
	TUNAPRECISIONLO toleranceFrequency=0.033;
	uint8_t minMatch=4;
	TUNAPRECISIONLO detFreq=0;
	TUNAPRECISIONLO detDiff=0;

*/






class analyseTuner : public AudioStream 
{
public:
	analyseTuner(void) : AudioStream(1, inputQueueArray) {
		// deploy tolerance ?
		
		
	}
	virtual void update(void);
	TUNAFLAGVARTYPE read(void); // the routine which does the calculations to determine tuning.
	TUNAPRECISIONLO tolSamples=0.025;
	TUNAPRECISIONLO tolFreqs=0.01;
	uint8_t minMatchPairs=4;
	
	TUNAPRECISIONLO detFreq=0;
	TUNAPRECISIONLO detDiff=0;

// here for compatibility with old code for now, not functional by any means.
	float tuning_scope=0.01; // was dividing, going to multiply now, 1% of detectedAverage might be the right scope.
	uint8_t minSamplePairs=3;
	uint8_t minMatchedSamples=2;

	
// protected:
	
	
	
private:
	audio_block_t *inputQueueArray[1];
	
	dp_t dp[2];
	
	TUNAPRECISIONLO SampleTol(TUNAPRECISIONLO n) { return n*tolSamples; }
	TUNAPRECISIONLO FreqTol(TUNAPRECISIONLO n) { return n*tolFreqs; }

	uint8_t nPtr(uint8_t n);
	TUNAFLAGVARTYPE flags=TUNAFRESHTUNA|TUNAFLAGTUNED|TUNAFLAGLOOSE|TUNAFLAGTIGHT|TUNAFLAGQUIET;

	const TUNAPRECISIONLO notes[12]={16.3515978312874,17.3239144360545,18.354047994838,19.4454364826301,20.6017223070544,21.8267644645627,
		23.1246514194771,24.4997147488593,25.9565435987466,27.5,29.1352350948806,30.8677063285078};
	// ratios normalising around notes[6] (where ratios[n]=notes[n]/notes[6])
	const TUNAPRECISIONLO ratios[12]={1.41421356237309,1.33483985417003,1.25992104989487,1.18920711500272,1.12246204830937,1.0594630943593,
		1,0.943874312681693,0.890898718140336,0.840896415253713,0.793700525984099,0.749153538438338};

	TUNAPRECISIONLO inSamples[2][8]={ {TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,
		TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD},
		{TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,
		TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD,TUNAINTERVALTHRESHOLD} };
		
	uint8_t sPtr[2]={0,0};
	
	uint8_t freshTunaFlag=0x0; // bit(0)=falling, bit(1)=rising
	
	TUNAPRECISIONLO pdSamples[8]={0,0,0,0,0,0,0,0};
	TUNAPRECISIONLO fAccum=0;

	
};

#endif

